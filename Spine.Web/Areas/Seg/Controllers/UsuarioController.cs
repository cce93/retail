﻿using Spine.Entidades.Seg;
using Spine.Seg.Negocio;
using Spine.Librerias.Autenticacion;
using Spine.Librerias.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Spine.Web.Areas.Seg.Controllers
{
    public class UsuarioController : WebBaseController
    {
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Autenticar(Usuario pobjUsuario)
        {
            try
            {
                Sesion vobjSesion = new UsuarioNeg(objSesion).Autenticar(pobjUsuario);

                // Eliminamos cookie
                Response.Cookies.Remove("SESION");

                // Creamos cookie
                HttpCookie vobjCookie = new HttpCookie("SESION");
                vobjCookie["SESION_TOKEN"] = "";
                vobjCookie.Expires = DateTime.Today.AddDays(1d);
                Response.Cookies.Add(vobjCookie);

                // Creamos session
                Session["SESION"] = vobjSesion;
                // Session[CnsWeb.SESION_MENU] = vsMenusHtml;




                return JsonExito(true);
            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }
        }

        [HttpGet]
        public ActionResult Consultar(int piUsuId = -1, string psUsuUsuario = "", string psUsuCorreo = "", short piSucId = -1, int piPerId = -1, short piUsuEstado = -1)
        {
            try
            {
                return JsonExito(new UsuarioNeg(objSesion).Consultar(piUsuId: piUsuId, psUsuUsuario: psUsuUsuario, psUsuCorreo: psUsuCorreo, piSucId: piSucId, piPerId: piPerId, piUsuEstado: piUsuEstado));
            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }
        }
    }
}