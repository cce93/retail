﻿using Spine.Constantes.Codigos.Cfg;
using Spine.Constantes.Valores.Cfg;
using Spine.Entidades.Cfg;
using Spine.Entidades.Cmn;
using Spine.Librerias.Web;
using Spine.Negocio.Cfg;
using Spine.Negocio.Cmn;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Spine.Web.Areas.Cmn.Controllers
{
    public class PersonaController : WebBaseController
    {
        // GET: Cmn/Persona
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Consultar(
            int piPerId = -1, short piPerTipo = -1, string psPerDoc = "", string psPerNombre = "",
            string psPerDocNombre = "", short piPerEstado = -1
        )
        {
            try
            {
                return JsonExito(new NegPersona(objSesion).Consultar(
                    piPerId: piPerId, piPerTipo: piPerTipo, psPerDoc: psPerDoc, psPerNombre: psPerNombre,
                    psPerDocNombre: psPerDocNombre, piPerEstado: piPerEstado
                ));
            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }
        }

        [HttpGet]
        public ActionResult MostrarCrear()
        {
            try
            {
                ViewBag.lstPerTipo = new NegCatalogo(objSesion).ConsultarItems(psCatCodigo: CodCatalogo.CMN_PERSONA_TIPO, piCatIteEstado: ValCatalogoItem.ESTADO_ACTIVADO);
                ViewBag.lstPerTipoDoc = new NegCatalogo(objSesion).ConsultarItems(psCatCodigo: CodCatalogo.CMN_PERSONA_TIPO_DOC, piCatIteEstado: ValCatalogoItem.ESTADO_ACTIVADO);
                ViewBag.objPersona = new Persona();
                ViewBag.lstPersonaTelefono = new List<PersonaInfo>();
                ViewBag.lstPersonaCorreo = new List<PersonaInfo>();
                ViewBag.lstPersonaDireccion = new List<PersonaInfo>();
                ViewBag.iAccion = 1;
            }
            catch (Exception ex)
            {
                throw;
            }
            return View("~/Areas/Cmn/Views/Persona/Form.cshtml");
        }

        [HttpGet]
        [Route("cmn/persona/mostrarEditar/{piPerId}")]
        public ActionResult MostrarEditar(int piPerId)
        {
            try
            {
                ViewBag.lstPerTipo = new NegCatalogo(objSesion).ConsultarItems(psCatCodigo: CodCatalogo.CMN_PERSONA_TIPO, piCatIteEstado: ValCatalogoItem.ESTADO_ACTIVADO);

                ViewBag.lstPerTipoDoc = new NegCatalogo(objSesion).ConsultarItems(psCatCodigo: CodCatalogo.CMN_PERSONA_TIPO_DOC, piCatIteEstado: ValCatalogoItem.ESTADO_ACTIVADO);

                ViewBag.objPersona = new NegPersona(objSesion).Consultar(piPerId: piPerId).FirstOrDefault();

                List<PersonaInfo> vlstPersonaInfo = new NegPersona(objSesion).ConsultarInfo(piPerId: piPerId);
                ViewBag.lstPersonaTelefono = vlstPersonaInfo.Where(x => x.iPerInfTipo == 1).ToList();
                ViewBag.lstPersonaCorreo = vlstPersonaInfo.Where(x => x.iPerInfTipo == 2).ToList();
                ViewBag.lstPersonaDireccion = vlstPersonaInfo.Where(x => x.iPerInfTipo == 3).ToList();

                ViewBag.iAccion = 2;
            }
            catch (Exception ex)
            {
                throw;
            }


            return null;
        }

    }
}