﻿using Spine.Negocio.Cmn;
using Spine.Librerias.Web;
using System;
using System.Web.Mvc;
using Spine.Negocio.Cfg;
using Spine.Constantes.Codigos.Cfg;
using Spine.Constantes.Valores.Cfg;
using Spine.Entidades.Cmn;

namespace Spine.Web.Areas.Cmn.Controllers
{
    public class CategoriaController : WebBaseController
    {
        [HttpGet]
        public ActionResult Index()
        {
            try
            {
                ViewBag.lstCtgTipo = new NegCatalogo(objSesion).ConsultarItems(psCatCodigo: CodCatalogo.CMN_PRODUCTO_TIPO, piCatIteEstado: ValCatalogoItem.ESTADO_ACTIVADO);
            }
            catch (Exception ex)
            {
                string ms = ex.Message;
            }



            return View();
        }

        [HttpGet]
        public ActionResult Consultar(short piCtgId = -1, string psCtgNombre = "", short piCtgTipo = -1, short piCtgEstado = -1)
        {
            try
            {
                return JsonExito(new NegCategoria(objSesion).Consultar(piCtgId: piCtgId, psCtgNombre: psCtgNombre, piCtgTipo: piCtgTipo, piCtgEstado: piCtgEstado));
            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }
        }

        [HttpPost]
        public ActionResult Crear(Categoria pobjCategoria)
        {
            try
            {
                return JsonExito(new NegCategoria(objSesion).Crear(pobjCategoria: pobjCategoria));
            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }
        }

        [HttpPut]
        public ActionResult Editar(Categoria pobjCategoria)
        {
            try
            {
                return JsonExito(new NegCategoria(objSesion).Editar(pobjCategoria: pobjCategoria));
            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }
        }

        [HttpPost]
        [Route("Cmn/Categoria/Activar/{piCtgId}")]
        public ActionResult Activar(short piCtgId)
        {
            try
            {
                return JsonExito(new NegCategoria(objSesion).Activar(piCtgId: piCtgId));
            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }
        }

        [HttpPost]
        [Route("Cmn/Categoria/Desactivar/{piCtgId}")]
        public ActionResult Desactivar(short piCtgId)
        {
            try
            {
                return JsonExito(new NegCategoria(objSesion).Desactivar(piCtgId: piCtgId));
            }
            catch (Exception ex)
            {
                return JsonError(ex);
            }
        }
    }
}