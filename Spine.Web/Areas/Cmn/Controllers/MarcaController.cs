﻿using Spine.Librerias.Web;
using Spine.Negocio.Cmn;
using System;
using System.Web.Mvc;

namespace Spine.Web.Areas.Cmn.Controllers
{
    public class MarcaController : WebBaseController
    {
        [HttpGet]
        public ActionResult Index()
        {
            try
            {
                //ViewBag.lstCtgTipo = new NegCatalogo(objSesion).ConsultarItems(psCatCodigo: CodCatalogo.CMN_PRODUCTO_TIPO, piCatIteEstado: ValCatalogoItem.ESTADO_ACTIVADO);
            }
            catch (Exception ex)
            {
                string ms = ex.Message;
            }

            return View();
        }

        [HttpGet]
        public ActionResult Consultar(short piMarId = -1, string psMarNombre = "", short piMarEstado = -1)
        {
            try
            {
                return JsonExito(new NegMarca(objSesion).Consultar(piMarId: piMarId, psMarNombre: psMarNombre, piMarEstado: piMarEstado));
            }
            catch (Exception vobjExcepcion)
            {
                return JsonError(vobjExcepcion);
            }
        }
    }
}