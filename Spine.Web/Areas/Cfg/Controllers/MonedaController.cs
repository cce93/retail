﻿using Spine.Negocio.Cfg;
using Spine.Librerias.Web;
using System;
using System.Web.Mvc;

namespace Spine.Web.Areas.Cfg.Controllers
{
    public class MonedaController : WebBaseController
    {
        [HttpGet]
        // [Autoriza]
        public ContentResult Consultar(short piMonId = -1, short piMonEstado = -1)
        {
            try
            {
                return JsonExito(new MonedaNeg(objSesion).Consultar(piMonId: piMonId, piMonEstado: piMonEstado));
            }
            catch (Exception vobjExcepcion)
            {
                return JsonError(vobjExcepcion);
            }
        }
    }
}