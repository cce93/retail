﻿using Spine.Negocio.Cfg;
using Spine.Librerias.Web;
using System;
using System.Web.Mvc;

namespace Spine.Web.Areas.Cfg.Controllers
{
    public class CatalogoController : WebBaseController
    {
        [HttpGet]
        public ContentResult ConsultarItems(string psCatCodigo, short piCatIteEstado = -1)
        {
            try
            {
                return JsonExito(new NegCatalogo(objSesion).ConsultarItems(psCatCodigo: psCatCodigo, piCatIteEstado: piCatIteEstado));
            }
            catch (Exception vobjExcepcion)
            {
                return JsonError(vobjExcepcion);
            }
        }


    }
}