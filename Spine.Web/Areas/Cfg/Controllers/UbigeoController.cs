﻿using Spine.Negocio.Cfg;
using Spine.Librerias.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Spine.Web.Areas.Cfg.Controllers
{
    public class UbigeoController : WebBaseController
    {
        [HttpGet]
        // [Autoriza]
        public ContentResult Consultar(short piUbiId = -1, string psUbiRuta = "", short piUbiTipo = -1, short piUbiEstado = -1)
        {
            try
            {
                return JsonExito(new UbigeoNeg(objSesion).Consultar(piUbiId: piUbiId, psUbiRuta: psUbiRuta, piUbiTipo: piUbiTipo, piUbiEstado: piUbiEstado));
            }
            catch (Exception vobjExcepcion)
            {
                return JsonError(vobjExcepcion);
            }
        }
    }
}