﻿using System.Web.Mvc;

namespace Spine.Web.Controllers
{
    public class ComunController : Controller
    {
        [HttpGet]
        public ActionResult Error(string psMensaje = "")
        {
            ViewData["sMensaje"] = psMensaje;
            return View();
        }

        [HttpGet]
        public ActionResult Expirado()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Bloqueado()
        {
            return View();
        }
    }
}