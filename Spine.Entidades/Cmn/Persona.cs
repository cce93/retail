﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace Spine.Entidades.Cmn
{
    public class Persona
    {
        public Persona() { }

        // Columnas
        [XmlAttribute] public int iPerId { set; get; }
        [XmlAttribute] public byte iPerTipo { set; get; }
        [XmlAttribute] public byte iPerTipoDoc { set; get; }
        [XmlAttribute] public string sPerDoc { set; get; }
        [XmlAttribute] public string sPerAtencion { set; get; }
        [XmlAttribute] public string sPerNombre { set; get; }
        [XmlAttribute] public string sPerApellidoPaterno { set; get; }
        [XmlAttribute] public string sPerApellidoMaterno { set; get; }
        [XmlAttribute] public string sPerNombres { set; get; }
        [XmlAttribute] public string sPerRazonSocial { set; get; }
        [XmlAttribute] public byte iPerEstado { set; get; }

        // Otros
        [XmlAttribute] public string sPerTipo { set; get; }
        [XmlAttribute] public string sPerTipoDoc { set; get; }
        [XmlAttribute] public string sPerEstado { set; get; }

        // Relaciones
        public List<PersonaInfo> lstPersonaInfo { set; get; }
    }
}
