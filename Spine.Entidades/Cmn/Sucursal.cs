﻿namespace Spine.Entidades.Cmn
{
    public class Sucursal
    {
        public Sucursal() { }

        // Columnas
        public short iSucId { set; get; }
        public string sSucNombre { set; get; }
        public short iUbiId { set; get; }
        public string sSucDireccion { set; get; }
        public byte iSucEstado { set; get; }

        // Otros
        public string sUbiNombre { set; get; }
        public string sSucEstado { set; get; }
    }
}