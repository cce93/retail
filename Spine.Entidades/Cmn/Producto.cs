﻿namespace Spine.Entidades.Cmn
{
    public class Producto
    {
        public int nIdProducto { set; get; }
        public byte nIdEmpresa { set; get; }
        public short nProTipo { set; get; }
        public short nProArticuloTipo { set; get; }
        public string sProArticuloTipo { set; get; }
        public string sProCodigo { set; get; }
        public string sProCodigoExterno { set; get; }
        public string sProNombre { set; get; }
        public string sProDescripcion { set; get; }
        public short nIdCategoria { set; get; }
        public short nIdMarca { set; get; }
        public int nIdInventario { set; get; }
        public short nProTipoOperacion { set; get; }
        public short nProGarantia { set; get; }
        public byte nIdUnidadMedida { set; get; }
        public decimal nProUtilidad { get; set; }
        public bool lProVenta { set; get; }
        public bool lProCompra { set; get; }
        public bool lProSerieObligatoria { set; get; }
        public byte nProEstado { set; get; }
        public bool lProUtilidad { set; get; }

        //OTROS
        public string sProTipo { set; get; }
        public string sProDenominacion { set; get; }
        public string sCatNombre { set; get; }
        public string sMarNombre { set; get; }
        public decimal nInvDisponible { set; get; }
        public string sUMeNombre { set; get; }
        public string sProEstado { set; get; }
        public decimal nPrePrecio { set; get; }
        public int nIdMoneda { set; get; }
        public decimal nTCaVenta { set; get; }
        public decimal nInvCantidad { set; get; }
        public string sPrePrecioSol { set; get; }
        public string sPrePrecioDolar { set; get; }
    }
}