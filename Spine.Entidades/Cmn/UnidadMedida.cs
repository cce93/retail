﻿namespace Spine.Entidades.Cmn
{
    public class UnidadMedida
    {
        public UnidadMedida() { }

        // Columnas
        public short iUniMedId { set; get; }
        public string sUniMedNombre { set; get; }
        public string sUMeSimbolo { set; get; }
        public string sUMeSimboloIso { set; get; }
        public byte iUniMedEstado { set; get; }

        // Otros
        public string sUMeEstado { set; get; }
    }
}
