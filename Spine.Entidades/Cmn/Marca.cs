﻿namespace Spine.Entidades.Cmn
{
    public class Marca
    {
        public Marca() { }

        // Columnas
        public short iMarId { set; get; }
        public string sMarNombre { set; get; }
        public byte iMarEstado { set; get; }

        // Otros
        public string sMarEstado { set; get; }
    }
}
