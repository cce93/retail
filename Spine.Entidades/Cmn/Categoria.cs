﻿namespace Spine.Entidades.Cmn
{
    public class Categoria
    {
        public Categoria() { }

        // Columnas
        public short iCtgId { get; set; }
        public string sCtgNombre { get; set; }
        public byte iCtgTipo { get; set; }
        public byte iCtgEstado { get; set; }

        // Otros
        public string sCtgTipo { get; set; }
        public string sCtgEstado { get; set; }
    }
}
