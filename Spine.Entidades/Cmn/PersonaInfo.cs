﻿using System.Xml.Serialization;

namespace Spine.Entidades.Cmn
{
    public class PersonaInfo
    {
        public PersonaInfo() { }

        // Columnas
        [XmlAttribute] public int iPerInfId { set; get; }
        [XmlAttribute] public int iPerId { set; get; }
        [XmlAttribute] public byte iPerInfTipo { set; get; }
        [XmlAttribute] public string sPerInfDescripcion { set; get; }
        [XmlAttribute] public string sPErInfValor { set; get; }
        [XmlAttribute] public short iUbiId { set; get; }
        [XmlAttribute] public string sPerInfReferencia { set; get; }
        [XmlAttribute] public bool lPerInfPrincipal { set; get; }
        [XmlAttribute] public byte iPerInfEstado { set; get; }

        // Otros
        [XmlAttribute] public string sPerInfTipo { set; get; }
        [XmlAttribute] public string sUbiNombre { set; get; }
        [XmlAttribute] public string sPerInfEstado { set; get; }
    }
}
