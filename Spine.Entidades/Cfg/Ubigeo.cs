﻿

namespace Spine.Entidades.Cfg
{
    public class Ubigeo
    {
        public short nIdUbigeo { set; get; }
        public short nIdUbigeoPadre { set; get; }
        public string sUbiCodigo { set; get; }
        public string sUbiNombre { set; get; }
        public string sUbiRuta { set; get; }
        public byte nUbiEstado { set; get; }
        public string sUbiEstado { set; get; }
    }
}
