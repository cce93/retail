﻿

namespace Spine.Entidades.Cfg
{
    public class Parametro
    {
        public short nIdParametro { set; get; }
        public byte nIdEmpresa { set; get; }
        public short nIdSucursal { set; get; }
        public string sParCodigo { set; get; }
        public string sParNombre { set; get; }
        public string sParDescripcion { set; get; }
        public string sParValor { set; get; }
        public string sParValor2 { set; get; }
        public byte nParEstado { set; get; }
        public string sParEstado { set; get; }
        public string sSucNombre { set; get; }
        public string sEmpRazonSocial { set; get; }
        public string sEmpRazonComercial { set; get; }
    }
}
