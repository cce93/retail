﻿namespace Spine.Entidades.Cfg
{
    public class CatalogoItem
    {
        public CatalogoItem() { }

        public short iCatIteId { set; get; }
        public short iCatId { set; get; }
        public byte iCatIteNum { set; get; }
        public string sCatIteNombre { set; get; }
        public byte iCatIteOrden { set; get; }
        public byte iCatIteEstado { set; get; }
        public string sCatIteEstado { set; get; }
    }
}
