﻿

namespace Spine.Entidades.Cfg
{
    public class Moneda
    {
        //Columnas
        public byte nIdMoneda { set; get; }
        public byte nIdEmpresa { set; get; }
        public string sMonNombre { set; get; }
        public string sMonNombrePlural { set; get; }
        public string sMonSimbolo { set; get; }
        public string sMonIso { set; get; }
        public byte nMonEstado { set; get; }

        //Otros
        public string sMonEstado { set; get; }
    }
}
