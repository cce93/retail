﻿using System.Collections.Generic;

namespace Spine.Entidades.Cfg
{
    public class Catalogo
    {
        public Catalogo() { }

        public short iCatId { set; get; }
        public byte iModId { set; get; }
        public string sCatCodigo { set; get; }
        public string sCatNombre { set; get; }
        public string sCatDescripcion { set; get; }
        public byte nCatEstado { set; get; }
        public string sCatEstado { set; get; }
        public List<CatalogoItem> lstCatalogoDet { set; get; }
    }
}
