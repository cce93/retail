﻿
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Spine.Entidades.Com
{
    public class FacturaItemVenta
    {
        //COLUMNAS
        [XmlAttribute]
        public long nIdComprobanteVentaLinea { get; set; }

        [XmlAttribute]
        public long nIdComprobanteVentaLineaPadre { set; get; }

        [XmlAttribute]
        public long nIdComprobanteVenta { get; set; }

        [XmlAttribute]
        public byte nCVLTipoOpe { get; set; }

        [XmlAttribute]
        public int nIdProducto { get; set; }

        [XmlAttribute]
        public short nProArticuloTipo { set; get; }

        [XmlAttribute]
        public int nCVLGarantiaProducto { get; set; }

        [XmlAttribute]
        public string sCVLSeries { get; set; }

        [XmlAttribute]
        public string sCVLSeriesDisponibles { get; set; }

        [XmlAttribute]
        public decimal nCVLCantidad { get; set; }

        [XmlAttribute]
        public decimal nCVLUnitarioValor { get; set; }

        [XmlAttribute]
        public decimal nCVLUnitarioDesct { get; set; }

        [XmlAttribute]
        public decimal nCVLUnitarioIgvBase { get; set; }

        [XmlAttribute]
        public decimal nCVLUnitarioIgv { get; set; }

        [XmlAttribute]
        public decimal nCVLUnitarioIscBase { get; set; }

        [XmlAttribute]
        public decimal nCVLUnitarioIsc { get; set; }

        [XmlAttribute]
        public decimal nCVLUnitarioPrecio { get; set; }

        [XmlAttribute]
        public decimal nCVLMontoValor { get; set; }

        [XmlAttribute]
        public decimal nCVLMontoDesct { get; set; }

        [XmlAttribute]
        public decimal nCVLMontoIgvBase { get; set; }

        [XmlAttribute]
        public decimal nCVLMontoIgv { get; set; }

        [XmlAttribute]
        public decimal nCVLMontoIscBase { get; set; }

        [XmlAttribute]
        public decimal nCVLMontoIsc { get; set; }

        [XmlAttribute]
        public decimal nCVLMontoSubtotal { get; set; }

        [XmlAttribute]
        public string sCVLDescripcion { get; set; }

        [XmlAttribute]
        public string sCVLDetalle { get; set; }

        [XmlAttribute]
        public decimal nCVLEntregado { get; set; }

        [XmlAttribute]
        public byte lCVLActivo { get; set; }
        [XmlAttribute]
        public long nIdAlmacenMovLineaRef { get; set; }

        //OTROS
        [XmlAttribute]
        public short nProTipo { get; set; }
        [XmlAttribute]
        public string sProNombre { get; set; }
        [XmlAttribute]
        public string sProNombreComercial { get; set; }
        [XmlAttribute]
        public string sProCodigo { get; set; }
        [XmlAttribute]
        public string sProCodigoExterno { get; set; }
        [XmlAttribute]
        public string sProDenominacion { get; set; }
        [XmlAttribute]
        public bool lProSerieObligatoria { get; set; }
        [XmlAttribute]
        public string sUMeNombre { get; set; }
        [XmlAttribute]
        public string sUMeSimbolo { get; set; }
        [XmlAttribute]
        public string sUMeSimboloSUNAT { get; set; }
        [XmlAttribute]
        public decimal nCVLCantTotal { set; get; }
        [XmlAttribute]
        public string sCVLTipoOpeNombre { get; set; }
        [XmlAttribute]
        public string sProDetalle { get; set; }
        public decimal nPrePrecioMin { get; set; }
        [XmlAttribute]
        public string sCVLSeriesEntregadas { get; set; }
        [XmlAttribute]
        public decimal nCVLCantidadPendiente { get; set; }
        [XmlAttribute]
        public string sCVLSeriesPendientes { get; set; }

        //RELACIONES
        public List<FacturaItemVenta> lstComprobanteVentaLinea { get; set; }
    }
}
