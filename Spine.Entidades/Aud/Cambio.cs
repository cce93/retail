﻿using System;

namespace Spine.Entidades.Aud
{
    public class Cambio
    {
        public Cambio() { }

        // Columnas
        public int iCamId { set; get; }
        public DateTime dCamFecha { set; get; }
        public int iUsuId { set; get; }
        public short iEntId { set; get; }
        public int iCamRegistro { set; get; }
        public byte iCamAccion { set; get; }
        public string sCamAccionOtro { set; get; }

        // Otros
        public string sEntCodigo { set; get; }
    }
}
