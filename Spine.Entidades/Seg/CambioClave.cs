﻿

namespace Spine.Entidades.Seg
{
    public class CambioClave
    {
        public int nIdUsuario { get; set; }
        public string sUsuClave { get; set; }
        public string sUsuClaveNueva { get; set; }
    }
}