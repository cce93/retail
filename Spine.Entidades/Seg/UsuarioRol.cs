﻿using System.Xml.Serialization;

namespace Spine.Entidades.Seg
{
    public class UsuarioRol
    {
        public UsuarioRol() { }

        // Columnas
        [XmlAttribute] public int iUsuRolId { set; get; }
        [XmlAttribute] public int iUsuId { set; get; }
        [XmlAttribute] public short iRolId { set; get; }
        [XmlAttribute] public byte iUsuRolEstado { set; get; }

        // Otros
        [XmlAttribute] public string sRolCodigo { set; get; }
        [XmlAttribute] public string sRolNombre { set; get; }
    }
}