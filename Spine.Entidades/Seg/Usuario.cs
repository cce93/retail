﻿namespace Spine.Entidades.Seg
{
    public class Usuario
    {
        public Usuario() { }

        // Columnas
        public int iUsuId { set; get; }
        public string sUsuUsuario { set; get; }
        public short iSucId { set; get; }
        public int iPerId { set; get; }
        public string sUsuClave { set; get; }
        public string sUsuCorreo { set; get; }
        public byte iUsuEstado { set; get; }

        // Otros
        public string sSucNombre { set; get; }
        public string sPerNombre { set; get; }
        public string sUsuEstado { set; get; }
        public string sUsuNuevaclave { set; get; }
    }
}