﻿

namespace Spine.Entidades.Seg
{
    public class Autenticacion
    {
        public byte nIdEmpresa { get; set; }
        public string sUsuUsuario { get; set; }
        public string sUsuClave { get; set; }
        public byte nSInCanal { get; set; }
        public string sSInDispositivo { get; set; }
    }
}
