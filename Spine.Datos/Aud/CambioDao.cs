﻿using Spine.Entidades.Aud;
using Spine.Librerias.Autenticacion;
using Spine.Librerias.Database;
using Spine.Librerias.Datos;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace Spine.Datos.Aud
{
    public class CambioDao : DaoBase
    {
        public CambioDao(Sesion pobjSesion) : base(pobjSesion)
        {
        }

        public int Crear(Conexion pobjConexion, Cambio pobjCambio)
        {
            SqlParameter[] varrParametros = new SqlParameter[] {
                new SqlParameter() { ParameterName = "@piCamId", Value = pobjCambio.iCamId, Direction = ParameterDirection.InputOutput},
                new SqlParameter("@pdCamFecha", pobjCambio.dCamFecha),
                new SqlParameter("@piUsuId", pobjCambio.iUsuId),
                new SqlParameter("@psEntCodigo", pobjCambio.sEntCodigo),
                new SqlParameter("@piCamRegistro", pobjCambio.iCamRegistro),
                new SqlParameter("@piCamAccion", pobjCambio.iCamAccion),
                new SqlParameter("@psCamAccionOtro", pobjCambio.sCamAccionOtro)
            };
            pobjConexion.Ejecutar("Aud.pa_Cambio_Crear", varrParametros);
            pobjCambio.iCamId = Convert.ToInt32(varrParametros.FirstOrDefault(x => x.ParameterName == "@piCamId").Value);
            return pobjCambio.iCamId;
        }

        public List<Cambio> Consultar(string psEntCodigo, int piCamRegistro)
        {
            using (var vobjConn = ConexionFactory.Instanciar())
            {
                return vobjConn.EjecutarConsulta<Cambio>(
                        "Aud.paF_Cambio",
                        new SqlParameter("@psEntCodigo", psEntCodigo),
                        new SqlParameter("@piCamRegistro", piCamRegistro)
                    );
            }
        }
    }
}
