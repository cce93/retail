﻿using Spine.Entidades.Cmn;
using Spine.Constantes.Codigos.Cfg;
using Spine.Librerias.Autenticacion;
using Spine.Librerias.Database;
using Spine.Librerias.Datos;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace Spine.Datos.Cmn
{
    public class DaoCategoria : DaoBase
    {
        public DaoCategoria(Sesion pobjSesion) : base(pobjSesion)
        {
        }

        
        public List<Categoria> Consultar(short piCtgId = -1, string psCtgNombre = "", short piCtgTipo = -1, short piCtgEstado = -1)
        {
            using (var vobjConexion = ConexionFactory.Instanciar())
            {
                return vobjConexion.EjecutarConsulta<Categoria>(
                    "Cmn.pa_Categoria_Consultar",
                    new SqlParameter("@piCtgId", piCtgId),
                    new SqlParameter("@psCtgNombre", psCtgNombre),
                    new SqlParameter("@piCtgTipo", piCtgTipo),
                    new SqlParameter("@piCtgEstado", piCtgEstado),
                    new SqlParameter("@psCatCodigoTipo", CodCatalogo.CMN_PRODUCTO_TIPO),
                    new SqlParameter("@psCatCodigoEstado", CodCatalogo.CMN_ESTADO)
                );
            }
        }

        public short Crear(Conexion pobjConexion, Categoria pobjCategoria)
        {
            SqlParameter[] varrParametros = new SqlParameter[] {
                new SqlParameter("@piCtgId", pobjCategoria.iCtgId){ Direction = ParameterDirection.Output },
                new SqlParameter("@piCtgTipo",  pobjCategoria.iCtgTipo),
                new SqlParameter("@psCtgNombre", pobjCategoria.sCtgNombre),
                new SqlParameter("@piCtgEstado", pobjCategoria.iCtgEstado)
            };
            pobjConexion.Ejecutar("Cmn.pa_Categoria_Crear", varrParametros);
            pobjCategoria.iCtgId = Convert.ToInt16(varrParametros.First(x => x.ParameterName == "@piCtgId").Value);
            return pobjCategoria.iCtgId;
        }

        public short Editar(Conexion pobjConexion, Categoria pobjCategoria)
        {
            pobjConexion.Ejecutar(
                "Cmn.pa_Categoria_Editar",
                new SqlParameter("@piCtgId", pobjCategoria.iCtgId),
                new SqlParameter("@piCtgTipo", pobjCategoria.iCtgTipo),
                new SqlParameter("@psCtgNombre", pobjCategoria.sCtgNombre)
            );
            return pobjCategoria.iCtgId;
        }

        public bool CambiarEstado(Conexion pobjConexion, short piCtgId, byte piCtgEstado)
        {
            return pobjConexion.Ejecutar(
                "Cmn.pa_Categoria_CambiarEstado",
                new SqlParameter("@piCtgId", piCtgId),
                new SqlParameter("@piCtgEstado", piCtgEstado)
            ) == 1;
        }
    }
}
