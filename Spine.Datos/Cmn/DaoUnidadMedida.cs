﻿using Spine.Entidades.Cmn;
using Spine.Librerias.Datos;
using Spine.Librerias.Autenticacion;
using System.Collections.Generic;
using Spine.Librerias.Database;
using System.Data.SqlClient;
using Spine.Constantes.Codigos.Cfg;
using System.Data;
using System;
using System.Linq;

namespace Spine.Datos.Cmn
{
    public class DaoUnidadMedida : DaoBase
    {
        public DaoUnidadMedida(Sesion pobjSesion) : base(pobjSesion)
        {
        }

        public List<UnidadMedida> Consultar(short piUniMedId = -1, string psUniMedNombre = "", short piUniMedEstado = -1)
        {
            using (var vobjConexion = ConexionFactory.Instanciar())
            {
                return vobjConexion.EjecutarConsulta<UnidadMedida>(
                    "Cmn.paUnidadMedidaConsultar",
                    new SqlParameter("@piUniMedId", piUniMedId),
                    new SqlParameter("@psUniMedNombre", psUniMedNombre),
                    new SqlParameter("@piUniMedEstado", piUniMedEstado),
                    new SqlParameter("@psCatCodigoEstado", CodCatalogo.CMN_ESTADO)
                );
            }
        }

        public short Crear(Conexion pobjConexion, UnidadMedida pobjUnidadMedida)
        {
            SqlParameter[] varrParametros = new SqlParameter[] {
                new SqlParameter(){ ParameterName = "@piUniMedId", Direction = ParameterDirection.Output },
                new SqlParameter(){ ParameterName = "@psUniMedNombre", Value = pobjUnidadMedida.sUniMedNombre },
                new SqlParameter(){ ParameterName = "@psUniMedSimbolo", Value = pobjUnidadMedida.sUMeSimbolo },
                new SqlParameter(){ ParameterName = "@psUMeSimboloIso", Value = pobjUnidadMedida.sUMeSimboloIso }
            };
            pobjConexion.Ejecutar("Cmn.paUnidadMedidaCrear", varrParametros);
            pobjUnidadMedida.iUniMedId = Convert.ToInt16(varrParametros.First(x => x.ParameterName == "@piUniMedId").Value);
            return pobjUnidadMedida.iUniMedId;
        }

        public short Editar(Conexion pobjConexion, UnidadMedida pobjUnidadMedida)
        {
            pobjConexion.Ejecutar(
                "Cmn.paUnidadMedidaEditar",
                new SqlParameter() { ParameterName = "@piUniMedId", Value = pobjUnidadMedida.iUniMedId },
                new SqlParameter() { ParameterName = "@psUniMedNombre", Value = pobjUnidadMedida.sUniMedNombre },
                new SqlParameter() { ParameterName = "@psUMeSimbolo", Value = pobjUnidadMedida.sUMeSimbolo },
                new SqlParameter() { ParameterName = "@psUMeSimboloIso", Value = pobjUnidadMedida.sUMeSimboloIso }
            );
            return pobjUnidadMedida.iUniMedId;
        }

        public bool CambiarEstado(Conexion pobjConexion, short piUniMedId, byte piUniMedEstado)
        {
            return pobjConexion.Ejecutar(
                "Cmn.paUnidadMedidaCambiar",
                new SqlParameter() { ParameterName = "@piUniMedId", Value = piUniMedId },
                new SqlParameter() { ParameterName = "@piUniMedEstado", Value = piUniMedEstado }
            ) == 1;
        }

    }
}
