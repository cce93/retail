﻿using Spine.Librerias.Datos;
using Spine.Librerias.Autenticacion;
using System.Collections.Generic;
using Spine.Entidades.Cmn;
using Spine.Librerias.Database;
using System.Data.SqlClient;
using Spine.Constantes.Codigos.Cfg;
using System.Data;
using System.Linq;
using System;

namespace Spine.Datos.Cmn
{
    public class DaoMarca : DaoBase
    {
        public DaoMarca(Sesion pobjSesion) : base(pobjSesion)
        {
        }

        public List<Marca> Consultar(short piMarId = -1, string psMarNombre = "", short piMarEstado = -1)
        {
            using (var vobjConexion = ConexionFactory.Instanciar())
            {
                return vobjConexion.EjecutarConsulta<Marca>(
                    "Cmn.paMarcaConsultar",
                    new SqlParameter("@piMarId", piMarId),
                    new SqlParameter("@psMarNombre", psMarNombre),
                    new SqlParameter("@piMarEstado", piMarEstado),
                    new SqlParameter("@psCatCodigoEstado", CodCatalogo.CMN_ESTADO)
                );
            }
        }

        public short Crear(Conexion pobjConexion, Marca pobjMarca)
        {
            SqlParameter[] varrParametros = new SqlParameter[] {
                new SqlParameter(){ ParameterName = "@piMarId", Direction = ParameterDirection.Output },
                new SqlParameter(){ ParameterName = "@psMarNombre", Value = pobjMarca.sMarNombre },
                new SqlParameter(){ ParameterName = "@piMarEstado", Value = pobjMarca.iMarEstado }
            };
            pobjConexion.Ejecutar("Cmn.paMarcaCrear", varrParametros);
            pobjMarca.iMarId = Convert.ToInt16(varrParametros.First(x => x.ParameterName == "@piMarId").Value);
            return pobjMarca.iMarId;
        }

        public short Editar(Conexion pobjConexion, Marca pobjMarca)
        {
            pobjConexion.Ejecutar(
                "Cmn.paMarcaEditar",
                new SqlParameter() { ParameterName = "@piMarId", Value = pobjMarca.iMarId },
                new SqlParameter() { ParameterName = "@psMarNombre", Value = pobjMarca.sMarNombre }
            );
            return pobjMarca.iMarId;
        }

        public bool CambiarEstado(Conexion pobjConexion, short piMarId, byte piMarEstado)
        {
            return pobjConexion.Ejecutar(
                "Cmn.paMarcaCambiarEstado",
                new SqlParameter() { ParameterName = "@piMarId", Value = piMarId },
                new SqlParameter() { ParameterName = "@piMarEstado", Value = piMarEstado }
            ) == 1;
        }
    }
}
