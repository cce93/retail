﻿using Spine.Constantes.Codigos.Cfg;
using Spine.Entidades.Cmn;
using Spine.Librerias.Autenticacion;
using Spine.Librerias.Database;
using Spine.Librerias.Datos;
using Spine.Librerias.General;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace Spine.Datos.Cmn
{
    public class DaoPersona : DaoBase
    {
        public DaoPersona(Sesion pobjSesion) : base(pobjSesion)
        {
        }

        public List<Persona> Consultar(int piPerId = -1, short piPerTipo = -1, string psPerDoc = "", string psPerNombre = "", string psPerDocNombre = "", short piPerEstado = -1)
        {
            using (var vobjConexion = ConexionFactory.Instanciar())
            {
                return vobjConexion.EjecutarConsulta<Persona>(
                    "Cmn.paPersonaConsultar",
                    new SqlParameter("@piPerId", piPerId),
                    new SqlParameter("@piPerTipo", piPerTipo),
                    new SqlParameter("@psPerDoc", psPerDoc),
                    new SqlParameter("@psPerNombre", psPerNombre),
                    new SqlParameter("@psPerDocNombre", psPerDocNombre),
                    new SqlParameter("@piPerEstado", piPerEstado),
                    new SqlParameter("@psCatCodigoTipo", CodCatalogo.CMN_PERSONA_TIPO),
                    new SqlParameter("@psCatCodigoTipoDoc", CodCatalogo.CMN_PERSONA_TIPO_DOC),
                    new SqlParameter("@psCatCodigoEstado", CodCatalogo.CMN_ESTADO)
                );
            }
        }

        public int Crear(Conexion pobjConexion, Persona pobjPersona)
        {
            SqlParameter[] varrParametros = new SqlParameter[] {
                new SqlParameter(){ ParameterName = "@piPerId", Direction = ParameterDirection.Output },
                new SqlParameter(){ ParameterName = "@pxPersona", Value = Metodos.ToJson(pobjPersona) }
            };
            pobjConexion.Ejecutar("Cmn.paPersonaCrear", varrParametros);
            pobjPersona.iPerId = Convert.ToInt32(varrParametros.First(x => x.ParameterName == "@piPerId").Value);
            return pobjPersona.iPerId;
        }

        public int Editar(Conexion pobjConexion, Persona pobjPersona)
        {
            pobjConexion.Ejecutar(
                "Cmn.paPersonaEditar",
                new SqlParameter() { ParameterName = "@pxPersona", Value = Metodos.ToJson(pobjPersona) }
            );
            return pobjPersona.iPerId;
        }

        public bool CambiarEstado(Conexion pobjConexion, int piPerId, byte piPerEstado)
        {
            return pobjConexion.Ejecutar(
                "Cmn.paPersonaCambiarEstado",
                new SqlParameter() { ParameterName = "@piPerId", Value = piPerId },
                new SqlParameter() { ParameterName = "@piPerEstado", Value = piPerEstado }
            ) == 1;
        }

        public List<PersonaInfo> ConsultarInfo(int piPerInfId = -1, int piPerId = -1, short piPerInfTipo = -1, short piPerInfPrincipal = -1, short piPerInfEstado = -1)
        {
            using (var vobjConexion = ConexionFactory.Instanciar())
            {
                return vobjConexion.EjecutarConsulta<PersonaInfo>(
                    "Cmn.paPersonaConsultarInfo",
                    new SqlParameter("@piPerInfId", piPerInfId),
                    new SqlParameter("@piPerId", piPerId),
                    new SqlParameter("@piPerInfTipo", piPerInfTipo),
                    new SqlParameter("@piPerInfPrincipal", piPerInfPrincipal),
                    new SqlParameter("@piPerInfEstado", piPerInfEstado),
                    new SqlParameter("@psCatCodigoTipo", CodCatalogo.CMN_PERSONA_INFO_TIPO),
                    new SqlParameter("@psCatCodigoEstado", CodCatalogo.CMN_ESTADO)
                );
            }
        }


    }
}
