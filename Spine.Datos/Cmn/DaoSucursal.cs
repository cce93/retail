﻿using Spine.Entidades.Cmn;
using Spine.Librerias.Datos;
using Spine.Librerias.Autenticacion;
using System.Collections.Generic;
using Spine.Librerias.Database;
using System.Data.SqlClient;
using Spine.Constantes.Codigos.Cfg;
using System.Data;
using System;
using System.Linq;

namespace Spine.Datos.Cmn
{
    public class DaoSucursal : DaoBase
    {
        public DaoSucursal(Sesion pobjSesion) : base(pobjSesion)
        {
        }

        public List<Sucursal> Consultar(short piSucId = -1, string psSucNombre = "", short piSucEstado = -1)
        {
            using (var vobjConexion = ConexionFactory.Instanciar())
            {
                return vobjConexion.EjecutarConsulta<Sucursal>(
                    "Cmn.paSucursalConsultar",
                    new SqlParameter("@piSucId", piSucId),
                    new SqlParameter("@psSucNombre", psSucNombre),
                    new SqlParameter("@piSucEstado", piSucEstado),
                    new SqlParameter("@psCatCodigoEstado", CodCatalogo.CMN_ESTADO)
                );
            }
        }

        public short Crear(Conexion pobjConexion, Sucursal pobjSucursal)
        {
            SqlParameter[] varrParametros = new SqlParameter[] {
                new SqlParameter(){ ParameterName = "@piSucId", Direction = ParameterDirection.Output },
                new SqlParameter(){ ParameterName = "@psSucNombre", Value = pobjSucursal.sSucNombre },
                new SqlParameter(){ ParameterName = "@piUbiId", Value = pobjSucursal.iUbiId },
                new SqlParameter(){ ParameterName = "@psSucDireccion", Value = pobjSucursal.sSucDireccion },
                new SqlParameter(){ ParameterName = "@piSucEstado", Value = pobjSucursal.iSucEstado }
            };
            pobjConexion.Ejecutar("Cmn.paSucursalCrear", varrParametros);
            pobjSucursal.iSucId = Convert.ToInt16(varrParametros.First(x => x.ParameterName == "@piSucId").Value);
            return pobjSucursal.iSucId;
        }

        public short Editar(Conexion pobjConexion, Sucursal pobjSucursal)
        {
            pobjConexion.Ejecutar(
                "Cmn.paSucursalEditar",
                new SqlParameter() { ParameterName = "@piSucId", Value = pobjSucursal.iSucId },
                new SqlParameter() { ParameterName = "@psSucNombre", Value = pobjSucursal.sSucNombre },
                new SqlParameter() { ParameterName = "@piUbiId", Value = pobjSucursal.iUbiId },
                new SqlParameter() { ParameterName = "@psSucDireccion", Value = pobjSucursal.sSucDireccion }
            );
            return pobjSucursal.iSucId;
        }

        public bool CambiarEstado(Conexion pobjConexion, short piSucId, byte piSucEstado)
        {
            return pobjConexion.Ejecutar(
                "Cmn.paSucursalCambiarEstado",
                new SqlParameter() { ParameterName = "@piSucId", Value = piSucId },
                new SqlParameter() { ParameterName = "@piSucEstado", Value = piSucEstado }
            ) == 1;
        }
    }
}
