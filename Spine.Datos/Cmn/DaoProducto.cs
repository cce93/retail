﻿using System;
using Spine.Entidades.Cmn;
using Spine.Librerias.Datos;
using System.Collections.Generic;
using Spine.Librerias.Autenticacion;

namespace Spine.Datos.Cmn
{
    public class DaoProducto : DaoBase
    {
        public DaoProducto(Sesion pobjSesion) : base(pobjSesion)
        {
        }
        //public List<StockSucursal> ConsultarStock(int pnIdProducto, byte pnIdEmpresa)
        //{
        //    List<StockSucursal> vLstSrockSucursal = new List<StockSucursal>();
        //    var voConexion = ConexionHelper.ConexionPredeterminada();
        //    try
        //    {
        //        voConexion.Open();
        //        vLstSrockSucursal = new Comando(voConexion).EjecutarConsultaProcedimiento<StockSucursal>(ObtenerProcedimiento("ConsultarStock"), pnIdProducto, pnIdEmpresa);
        //    }
        //    catch (Exception vobjExcepcion)
        //    {
        //        if (!Metodos.EsValidacion(vobjExcepcion))
        //        {
        //            Tracer.GuardarError(objSesionInfo, vobjExcepcion, "DTipoCodigo.ConsultarStock()", pnIdProducto, pnIdEmpresa);
        //            throw vobjExcepcion;
        //        }
        //    }
        //    finally
        //    {
        //        voConexion.Close();
        //    }
        //    return vLstSrockSucursal;
        //}
        //public List<Inventario> ConsultarInventario(
        //    int pnIdInventario = -1, short pnIdEmpresa = -1, short pnIdSucursal = -1, short pnIdAlmacen = -1,
        //    int pnIdProducto = -1, short pnProTipo = -1, short pnProArticuloTipo = -1, short pnProVenta = -1,
        //    short pnProCompra = -1, string psProNombre = "", short pnAlmVenta = -1, byte pnConsultaTipo = 1
        //)
        //{
        //    List<Inventario> vlstInventario = new List<Inventario>();
        //    Conexion vobjConexion = null;
        //    try
        //    {
        //        string vsProcedimiento = ObtenerProcedimiento("ConsultarInventario");
        //        vobjConexion = ConexionHelper.ConexionPredeterminada();
        //        vobjConexion.Open();
        //        vlstInventario = new Comando(vobjConexion).EjecutarConsultaProcedimiento<Inventario>(
        //            vsProcedimiento, pnIdInventario, pnIdEmpresa, pnIdSucursal, pnIdAlmacen,
        //            pnIdProducto, pnProTipo, pnProArticuloTipo, pnProVenta,
        //            pnProCompra, psProNombre, pnAlmVenta, pnConsultaTipo
        //        );
        //    }
        //    catch (Exception vobjExcepcion)
        //    {
        //        Tracer.GuardarError(objSesionInfo, vobjExcepcion, "DProducto.ConsultarInventario()",
        //            pnIdInventario, pnIdProducto, pnIdEmpresa, pnIdSucursal,
        //            pnIdAlmacen, pnProArticuloTipo, psProNombre, pnAlmVenta);
        //        throw vobjExcepcion;
        //    }
        //    finally
        //    {
        //        if (vobjConexion != null)
        //            vobjConexion.Close();
        //    }
        //    return vlstInventario;
        //}
        //public List<Precio> ConsultarPrecio(int pnIdPrecio = -1, short pnIdEmpresa = -1, short pnIdSucursal = -1, short pnIdMoneda = -1, short pnIdProducto = -1, short pnPreTipo = -1, short pnPreEstado = -1, short pnConsultaTipo = 1)
        //{
        //    List<Precio> vlstPrecio = new List<Precio>();
        //    var voConexion = ConexionHelper.ConexionPredeterminada();
        //    try
        //    {
        //        voConexion.Open();
        //        vlstPrecio = new Comando(voConexion).EjecutarConsultaProcedimiento<Precio>(
        //            ObtenerProcedimiento("ConsultarPrecio"),
        //            pnIdPrecio,
        //            pnIdEmpresa,
        //            pnIdSucursal,
        //            pnIdMoneda,
        //            pnIdProducto,
        //            pnPreTipo,
        //            pnPreEstado,
        //            pnConsultaTipo);
        //    }
        //    catch (Exception vobjExcepcion)
        //    {
        //        if (!Metodos.EsValidacion(vobjExcepcion))
        //        {
        //            Tracer.GuardarError(objSesionInfo, vobjExcepcion, "DProducto.ConsultarPrecio()", pnIdPrecio, pnIdEmpresa, pnIdSucursal, pnIdMoneda, pnIdProducto, pnPreTipo, pnPreEstado, pnConsultaTipo);
        //            throw vobjExcepcion;
        //        }
        //    }
        //    finally
        //    {
        //        voConexion.Close();
        //    }
        //    return vlstPrecio;
        //}
        //public bool GuardarPrecio(Precio pobjPrecio)
        //{
        //    Conexion vobjConexion = null;
        //    try
        //    {
        //        string vsProcedimiento = ObtenerProcedimiento("GuardarPrecio");
        //        vobjConexion = ConexionHelper.ConexionPredeterminada();
        //        vobjConexion.Open();
        //        vobjConexion.BeginTransaction();
        //        int nRespuesta = new Comando(vobjConexion).EjecutarProcedimiento(
        //            vsProcedimiento,
        //            pobjPrecio.nIdPrecio,
        //            pobjPrecio.nIdEmpresa,
        //            pobjPrecio.nIdUsuario,
        //            pobjPrecio.nIdSucursal,
        //            pobjPrecio.nIdProducto,
        //            pobjPrecio.nPreTipo,
        //            pobjPrecio.nIdMoneda,
        //            pobjPrecio.nPrePrecio,
        //            pobjPrecio.nPrePrecioMin,
        //            pobjPrecio.nPreDescuento,
        //            Metodos.FechaSistema(),
        //            objSesionInfo.nIdSesion
        //        );
        //        vobjConexion.Commit();
        //    }
        //    catch (Exception vobjExcepcion)
        //    {
        //        if (vobjConexion != null)
        //            vobjConexion.Rollback();
        //        Tracer.GuardarError(objSesionInfo, vobjExcepcion, "DProducto.GuardarPrecio()", pobjPrecio);
        //        throw vobjExcepcion;
        //    }
        //    finally
        //    {
        //        if (vobjConexion != null)
        //            vobjConexion.Close();
        //    }
        //    return true;
        //}
        //public bool ValidarGuardarPrecio(Precio poPrecio)
        //{
        //    Conexion voConexion = ConexionHelper.ConexionPredeterminada();
        //    try
        //    {
        //        voConexion.Open();
        //        voConexion.BeginTransaction();
        //        int nRespuesta = new Comando(voConexion).EjecutarProcedimiento(
        //            ObtenerProcedimiento("ValidarGuardarPrecio"),
        //            poPrecio.nIdPrecio,
        //            poPrecio.nIdEmpresa,
        //            poPrecio.nIdSucursal,
        //            poPrecio.nIdProducto,
        //            poPrecio.nIdMoneda,
        //            poPrecio.nPreTipo
        //        //poPrecio.nPrePrecio,
        //        //poPrecio.nPrePrecioMin,
        //        //poPrecio.nPreDescuento
        //        );
        //        voConexion.Commit();
        //    }
        //    catch (Exception vobjExcepcion)
        //    {
        //        voConexion.Rollback();
        //        Tracer.GuardarError(objSesionInfo, vobjExcepcion, "DPrecio.ValidarGuardarPrecio()", poPrecio);
        //        throw vobjExcepcion;
        //    }
        //    finally
        //    {
        //        voConexion.Close();
        //    }
        //    return true;
        //}
        //public bool GuardarInventario(Inventario pobjInventario)
        //{
        //    Conexion vobjConexion = null;
        //    try
        //    {
        //        string vsProcedimiento = ObtenerProcedimiento("GuardarInventario");
        //        vobjConexion = ConexionHelper.ConexionPredeterminada();
        //        vobjConexion.Open();
        //        vobjConexion.BeginTransaction();
        //        new Comando(vobjConexion).EjecutarProcedimiento(
        //            vsProcedimiento,
        //            pobjInventario.nIdInventario,
        //            pobjInventario.nInvInicial,
        //            Metodos.FechaSistema(),
        //            objSesionInfo.nIdSesion
        //        );
        //        vobjConexion.Commit();
        //    }
        //    catch (Exception vobjExcepcion)
        //    {
        //        if (vobjConexion != null)
        //            vobjConexion.Rollback();
        //        Tracer.GuardarError(objSesionInfo, vobjExcepcion, "DProducto.GuardarInventario()", pobjInventario);
        //        throw vobjExcepcion;
        //    }
        //    finally
        //    {
        //        if (vobjConexion != null)
        //            vobjConexion.Close();
        //    }
        //    return true;
        //}
        //public List<Kardex> ConsultarKardex(int pnIdProducto, short pnIdAlmacen)
        //{
        //    List<Kardex> vlstKardex = new List<Kardex>();
        //    var vobjConexion = ConexionHelper.ConexionPredeterminada();
        //    try
        //    {
        //        vobjConexion.Open();
        //        vlstKardex = new Comando(vobjConexion).EjecutarConsultaProcedimiento<Kardex>(
        //            ObtenerProcedimiento("ConsultarKardex"),
        //            pnIdProducto,
        //            pnIdAlmacen
        //            );
        //    }
        //    catch (Exception vobjExcepcion)
        //    {
        //        Tracer.GuardarError(objSesionInfo, vobjExcepcion, "DProducto.ConsultarKardex()", pnIdProducto);
        //        throw vobjExcepcion;
        //    }
        //    finally
        //    {
        //        if (vobjConexion != null)
        //            vobjConexion.Close();
        //    }
        //    return vlstKardex;
        //}
    }
}