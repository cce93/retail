﻿using Spine.Librerias.Datos;
using Spine.Librerias.Autenticacion;
using Spine.Entidades.Seg;
using Spine.Librerias.Database;
using System.Data.SqlClient;
using System.Linq;
using System.Collections.Generic;
using Spine.Constantes.Codigos.Cfg;

namespace Spine.Datos.Seg
{
    public class UsuarioDao : DaoBase
    {
        public UsuarioDao(Sesion pobjSesion) : base(pobjSesion)
        {
        }

        public Sesion Autenticar(Usuario pobjUsuario)
        {
            using (var vobjDatabaseHelper = ConexionFactory.Instanciar())
            {
                return vobjDatabaseHelper.EjecutarConsulta<Sesion>(
                    "Seg.paUsuarioAutenticar",
                    new SqlParameter("@psUsuUsuario", pobjUsuario.sUsuUsuario),
                    new SqlParameter("@psUsuClave", pobjUsuario.sUsuClave)
                ).First();
            }
        }

        public bool CambiarClave(Conexion vobjConexion, Usuario pobjUsuario)
        {
            return vobjConexion.Ejecutar(
                    "Seg.paUsuarioCambiarClave",
                    new SqlParameter("@piUsuId", pobjUsuario.sUsuUsuario),
                    new SqlParameter("@psUsuClave", pobjUsuario.sUsuNuevaclave)
                ) == 1;
        }

        public bool ReiniciarClave(Conexion vobjConexion, Usuario pobjUsuario)
        {
            return vobjConexion.Ejecutar(
                    "Seg.paUsuarioReiniciarClave",
                    new SqlParameter("@piUsuId", pobjUsuario.iUsuId),
                    new SqlParameter("@psUsuClave", pobjUsuario.sUsuNuevaclave)
                ) == 1;
        }

        public List<Usuario> Consultar(int piUsuId = -1, string psUsuUsuario = "", string psUsuCorreo = "", string psUsuClave = "", short piSucId = -1, int piPerId = -1, short piUsuEstado = -1)
        {
            using (var vobjConexion = ConexionFactory.Instanciar())
            {
                return vobjConexion.EjecutarConsulta<Usuario>(
                    "Seg.paUsuarioConsultar",
                    new SqlParameter("@piUsuId", piUsuId),
                    new SqlParameter("@psUsuUsuario", psUsuUsuario),
                    new SqlParameter("@psUsuCorreo", psUsuCorreo),
                    new SqlParameter("@psUsuClave", psUsuClave),
                    new SqlParameter("@piSucId", piSucId),
                    new SqlParameter("@piPerId", piPerId),
                    new SqlParameter("@piUsuEstado", piUsuEstado),
                    new SqlParameter("@psCatCodigoEstado", CodCatalogo.CMN_ESTADO)
                );
            }
        }

    }
}