﻿using Spine.Entidades.Cfg;
using Spine.Librerias.Datos;
using Spine.Librerias.Autenticacion;
using System.Collections.Generic;
using System.Data.SqlClient;
using Spine.Librerias.Database;

namespace Spine.Datos.Cfg
{

    public class DaoMoneda : DaoBase
    {
        public DaoMoneda(Sesion pobjSesion) : base(pobjSesion)
        {
        }

        public List<Moneda> Consultar(short piMonId = -1, short piMonEstado = -1)
        {
            using (var vobjConexion = ConexionFactory.Instanciar())
            {
                return vobjConexion.EjecutarConsulta<Moneda>(
                        "Cfg.paMonedaConsultar",
                        new SqlParameter("@piMonId", piMonId),
                        new SqlParameter("@piMonEstado", piMonEstado)
                    );
            }
        }
    }
}
