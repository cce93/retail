﻿using Spine.Entidades.Cfg;
using Spine.Librerias.Datos;
using Spine.Librerias.Autenticacion;
using Spine.Librerias.Database;
using System.Data.SqlClient;
using System.Linq;

namespace Spine.Datos.Cfg
{

    public class DaoParametro : DaoBase
    {
        public DaoParametro(Sesion pobjSesion) : base(pobjSesion)
        {
        }

        public Parametro ConsultarUno(string psParCodigo)
        {
            using (var vobjConn = ConexionFactory.Instanciar())
            {
                return vobjConn.EjecutarConsulta<Parametro>(
                        "Cfg.paParametroConsultar",
                        new SqlParameter("@psParCodigo", psParCodigo)
                    ).First();
            }
        }
    }
}
