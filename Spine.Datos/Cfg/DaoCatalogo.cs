﻿using Spine.Entidades.Cfg;
using Spine.Librerias.Datos;
using Spine.Librerias.Autenticacion;
using System.Collections.Generic;
using Spine.Librerias.Database;
using System.Data.SqlClient;

namespace Spine.Datos.Cfg
{

    public class DaoCatalogo : DaoBase
    {
        public DaoCatalogo(Sesion pobjSesion) : base(pobjSesion)
        {
        }

        public List<Catalogo> Consultar(int piCatId = -1, string psCatCodigo = "", sbyte piCatEstado = -1)
        {
            using (var vobjConn = ConexionFactory.Instanciar())
            {
                return vobjConn.EjecutarConsulta<Catalogo>(
                        "Cfg.paCatalogoConsultar",
                        new SqlParameter("@piCatId", piCatId),
                        new SqlParameter("@psCatCodigo", psCatCodigo),
                        new SqlParameter("@piCatEstado", piCatEstado)
                    );
            }
        }

        public List<CatalogoItem> ConsultarItems(string psCatCodigo, short piCatIteEstado = -1)
        {
            using (var vobjConn = ConexionFactory.Instanciar())
            {
                return vobjConn.EjecutarConsulta<CatalogoItem>(
                        "Cfg.paCatalogoConsultarItems",
                        new SqlParameter("@psCatCodigo", psCatCodigo),
                        new SqlParameter("@piCatIteEstado", piCatIteEstado)
                    );
            }
        }
    }
}
