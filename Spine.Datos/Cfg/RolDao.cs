﻿using Spine.Entidades.Cfg;
using Spine.Entidades.Seg;
using Spine.Librerias.Autenticacion;
using Spine.Librerias.Database;
using Spine.Librerias.Datos;
using System.Collections.Generic;

namespace Spine.Datos.Cfg
{
    public class RolDao : DaoBase
    {
        public RolDao(Sesion pobjSesion) : base(pobjSesion)
        {
        }

        public List<Rol> Consultar(short piRolId = -1, string psRolCodigo = "", short piRolEstado = -1)
        {
            return null;
        }

        public bool Activar(Conexion pobjConn, short piRolId)
        {
            return false;
        }

        public bool Desactivar(Conexion pobjConn, short piRolId)
        {
            return false;
        }

        public List<RolOpcion> ConsultarOpciones(short piRolId)
        {
            return null;
        }
    }
}
