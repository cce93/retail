﻿using Spine.Entidades.Cfg;
using Spine.Librerias.Datos;
using Spine.Librerias.Autenticacion;
using Spine.Librerias.Database;
using System.Data.SqlClient;
using System.Linq;
using System.Collections.Generic;
using Spine.Constantes.Codigos.Cfg;

namespace Spine.Datos.Cfg
{

    public class DaoEmpresa : DaoBase
    {
        public DaoEmpresa(Sesion pobjSesion) : base(pobjSesion)
        {
        }

        public List<Empresa> Consultar(short piEmpId = -1, string psEmpRuc = "", short piEmpEstado = -1)
        {
            using (var vobjConn = ConexionFactory.Instanciar())
            {
                return vobjConn.EjecutarConsulta<Empresa>(
                        "Cfg.paEmpresaConsultar",
                        new SqlParameter("@piEmpId", piEmpId),
                        new SqlParameter("@psEmpRuc", psEmpRuc),
                        new SqlParameter("@piEmpEstado", piEmpEstado),
                        new SqlParameter("@psCatCodigoEstado", CodCatalogo.CMN_ESTADO)
                    );
            }
        }
    }
}
