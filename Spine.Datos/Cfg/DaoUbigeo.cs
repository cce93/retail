﻿using Spine.Entidades.Cfg;
using Spine.Librerias.Datos;
using Spine.Librerias.Autenticacion;
using System.Collections.Generic;
using Spine.Librerias.Database;
using System.Data.SqlClient;

namespace Spine.Datos.Cfg
{

    public class DaoUbigeo : DaoBase
    {
        public DaoUbigeo(Sesion pobjSesion) : base(pobjSesion)
        {
        }

        public List<Ubigeo> Consultar(short piUbiId = -1, string psUbiRuta = "", short piUbiTipo = -1, short piUbiEstado = -1)
        {
            using (var vobjConn = ConexionFactory.Instanciar())
            {
                return vobjConn.EjecutarConsulta<Ubigeo>(
                        "Cfg.paUbigeoConsultar",
                        new SqlParameter("@piUbiId", piUbiId),
                        new SqlParameter("@psUbiRuta", psUbiRuta),
                        new SqlParameter("@piUbiTipo", piUbiTipo),
                        new SqlParameter("@piUbiEstado", piUbiEstado)
                    );
            }
        }
    }
}
