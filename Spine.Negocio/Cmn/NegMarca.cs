﻿using Spine.Constantes.Codigos.Cfg;
using Spine.Constantes.Valores.Cfg;
using Spine.Datos.Aud;
using Spine.Datos.Cmn;
using Spine.Entidades.Aud;
using Spine.Entidades.Cmn;
using Spine.Librerias.Autenticacion;
using Spine.Librerias.Database;
using Spine.Librerias.General;
using Spine.Librerias.Negocio;
using System.Collections.Generic;

namespace Spine.Negocio.Cmn
{
    public class NegMarca : NegBase
    {
        public NegMarca(Sesion pobjSesion) : base(pobjSesion)
        {
        }

        public List<Marca> Consultar(short piMarId = -1, string psMarNombre = "", short piMarEstado = -1)
        {
            return new DaoMarca(objSesion).Consultar(piMarId: piMarId, psMarNombre: psMarNombre, piMarEstado: piMarEstado);
        }

        public short Crear(Marca pobjMarca)
        {
            using (Conexion vobjConexion = ConexionFactory.Instanciar())
            {
                vobjConexion.BeginTransaction();

                new DaoMarca(objSesion).Crear(vobjConexion, pobjMarca);

                Cambio vobjCambio = new Cambio();
                vobjCambio.dCamFecha = Metodos.FechaSistema();
                vobjCambio.iUsuId = objSesion.iUsuId;
                vobjCambio.sEntCodigo = EntidadCod.MARCA;
                vobjCambio.iCamRegistro = pobjMarca.iMarId;
                vobjCambio.iCamAccion = ValCatalogoItem.ACCION_CREACION;
                new CambioDao(objSesion).Crear(vobjConexion, vobjCambio);

                vobjConexion.Commit();

                return pobjMarca.iMarId;
            }
        }

        public short Editar(Marca pobjMarca)
        {
            using (Conexion vobjConexion = ConexionFactory.Instanciar())
            {
                vobjConexion.BeginTransaction();

                new DaoMarca(objSesion).Editar(vobjConexion, pobjMarca);

                Cambio vobjCambio = new Cambio();
                vobjCambio.dCamFecha = Metodos.FechaSistema();
                vobjCambio.iUsuId = objSesion.iUsuId;
                vobjCambio.sEntCodigo = EntidadCod.MARCA;
                vobjCambio.iCamRegistro = pobjMarca.iMarId;
                vobjCambio.iCamAccion = ValCatalogoItem.ACCION_EDICION;
                new CambioDao(objSesion).Crear(vobjConexion, vobjCambio);

                vobjConexion.Commit();

                return pobjMarca.iMarId;
            }
        }

        public bool Activar(short piMarId)
        {
            using (Conexion vobjConexion = ConexionFactory.Instanciar())
            {
                vobjConexion.BeginTransaction();

                new DaoMarca(objSesion).CambiarEstado(vobjConexion, piMarId, ValCatalogoItem.ESTADO_ACTIVADO);

                Cambio vobjCambio = new Cambio();
                vobjCambio.dCamFecha = Metodos.FechaSistema();
                vobjCambio.iUsuId = objSesion.iUsuId;
                vobjCambio.sEntCodigo = EntidadCod.MARCA;
                vobjCambio.iCamRegistro = piMarId;
                vobjCambio.iCamAccion = ValCatalogoItem.ACCION_ACTIVACION;
                new CambioDao(objSesion).Crear(vobjConexion, vobjCambio);

                vobjConexion.Commit();

                return true;
            }
        }

        public bool Desactivar(short piMarId)
        {
            using (Conexion vobjConexion = ConexionFactory.Instanciar())
            {
                vobjConexion.BeginTransaction();

                new DaoMarca(objSesion).CambiarEstado(vobjConexion, piMarId, ValCatalogoItem.ESTADO_DESACTIVADO);

                Cambio vobjCambio = new Cambio();
                vobjCambio.dCamFecha = Metodos.FechaSistema();
                vobjCambio.iUsuId = objSesion.iUsuId;
                vobjCambio.sEntCodigo = EntidadCod.MARCA;
                vobjCambio.iCamRegistro = piMarId;
                vobjCambio.iCamAccion = ValCatalogoItem.ACCION_DESACTIVACION;
                new CambioDao(objSesion).Crear(vobjConexion, vobjCambio);

                vobjConexion.Commit();

                return true;
            }
        }
    }
}
