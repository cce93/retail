﻿using Spine.Datos.Cmn;
using Spine.Entidades.Cmn;
using Spine.Librerias.Autenticacion;
using Spine.Librerias.Negocio;
using System.Collections.Generic;

namespace Spine.Negocio.Cmn
{
    public class NegPersona : NegBase
    {
        public NegPersona(Sesion pobjSesion) : base(pobjSesion)
        {
        }

        public List<Persona> Consultar(
            int piPerId = -1, short piPerTipo = -1, string psPerDoc = "", string psPerNombre = "",
            string psPerDocNombre = "", short piPerEstado = -1)
        {
            return new DaoPersona(objSesion).Consultar(
                piPerId: piPerId, piPerTipo: piPerTipo, psPerDoc: psPerDoc, psPerNombre: psPerNombre,
                psPerDocNombre: psPerDocNombre, piPerEstado: piPerEstado
            );
        }

        public List<PersonaInfo> ConsultarInfo(int piPerInfId = -1, int piPerId = -1, short piPerInfTipo = -1, short piPerInfPred = -1, short piPerInfEstado = -1)
        {
            return new DaoPersona(objSesion).ConsultarInfo(piPerInfId: piPerInfId, piPerId: piPerId, piPerInfTipo: piPerInfTipo, piPerInfPrincipal: piPerInfPred, piPerInfEstado: piPerInfEstado);
        }
    }
}
