﻿using Spine.Entidades.Cmn;
using Spine.Librerias.Negocio;
using Spine.Librerias.Autenticacion;
using Spine.Librerias.Database;
using Spine.Datos.Cmn;
using Spine.Datos.Aud;
using Spine.Entidades.Aud;
using System.Collections.Generic;
using Spine.Librerias.General;
using Spine.Constantes.Codigos.Cfg;
using Spine.Constantes.Valores.Cfg;

namespace Spine.Negocio.Cmn
{
    public class NegUnidadMedida : NegBase
    {
        public NegUnidadMedida(Sesion pobjSesion) : base(pobjSesion)
        {
        }

        public List<UnidadMedida> Consultar(short piUniMedId = -1, string psUniMedNombre = "", short piUniMedEstado = -1)
        {
            return new DaoUnidadMedida(objSesion).Consultar(piUniMedId: piUniMedId, psUniMedNombre: psUniMedNombre, piUniMedEstado: piUniMedEstado);
        }

        public short Crear(UnidadMedida pobjUnidadMedida)
        {
            using (Conexion vobjConexion = ConexionFactory.Instanciar())
            {
                vobjConexion.BeginTransaction();

                new DaoUnidadMedida(objSesion).Crear(vobjConexion, pobjUnidadMedida);

                Cambio vobjCambio = new Cambio();
                vobjCambio.dCamFecha = Metodos.FechaSistema();
                vobjCambio.iUsuId = objSesion.iUsuId;
                vobjCambio.sEntCodigo = EntidadCod.UNIDAD_MEDIDA;
                vobjCambio.iCamRegistro = pobjUnidadMedida.iUniMedId;
                vobjCambio.iCamAccion = ValCatalogoItem.ACCION_CREACION;
                new CambioDao(objSesion).Crear(vobjConexion, vobjCambio);

                vobjConexion.Commit();

                return pobjUnidadMedida.iUniMedId;
            }
        }

        public short Editar(UnidadMedida pobjUnidadMedida)
        {
            using (Conexion vobjConexion = ConexionFactory.Instanciar())
            {
                vobjConexion.BeginTransaction();

                new DaoUnidadMedida(objSesion).Editar(vobjConexion, pobjUnidadMedida);

                Cambio vobjCambio = new Cambio();
                vobjCambio.dCamFecha = Metodos.FechaSistema();
                vobjCambio.iUsuId = objSesion.iUsuId;
                vobjCambio.sEntCodigo = EntidadCod.UNIDAD_MEDIDA;
                vobjCambio.iCamRegistro = pobjUnidadMedida.iUniMedId;
                vobjCambio.iCamAccion = ValCatalogoItem.ACCION_EDICION;
                new CambioDao(objSesion).Crear(vobjConexion, vobjCambio);

                vobjConexion.Commit();

                return pobjUnidadMedida.iUniMedId;
            }
        }

        public bool Activar(short piMarId)
        {
            using (Conexion vobjConexion = ConexionFactory.Instanciar())
            {
                vobjConexion.BeginTransaction();

                new DaoUnidadMedida(objSesion).CambiarEstado(vobjConexion, piMarId, ValCatalogoItem.ESTADO_ACTIVADO);

                Cambio vobjCambio = new Cambio();
                vobjCambio.dCamFecha = Metodos.FechaSistema();
                vobjCambio.iUsuId = objSesion.iUsuId;
                vobjCambio.sEntCodigo = EntidadCod.UNIDAD_MEDIDA;
                vobjCambio.iCamRegistro = piMarId;
                vobjCambio.iCamAccion = ValCatalogoItem.ACCION_ACTIVACION;
                new CambioDao(objSesion).Crear(vobjConexion, vobjCambio);

                vobjConexion.Commit();

                return true;
            }
        }

        public bool Desactivar(short piUnidadMedida)
        {
            using (Conexion vobjConexion = ConexionFactory.Instanciar())
            {
                vobjConexion.BeginTransaction();

                new DaoUnidadMedida(objSesion).CambiarEstado(vobjConexion, piUnidadMedida, ValCatalogoItem.ESTADO_DESACTIVADO);

                Cambio vobjCambio = new Cambio();
                vobjCambio.dCamFecha = Metodos.FechaSistema();
                vobjCambio.iUsuId = objSesion.iUsuId;
                vobjCambio.sEntCodigo = EntidadCod.UNIDAD_MEDIDA;
                vobjCambio.iCamRegistro = piUnidadMedida;
                vobjCambio.iCamAccion = ValCatalogoItem.ACCION_DESACTIVACION;
                new CambioDao(objSesion).Crear(vobjConexion, vobjCambio);

                vobjConexion.Commit();

                return true;
            }
        }

    }
}
