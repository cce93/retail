﻿using Spine.Datos.Aud;
using Spine.Datos.Cmn;
using Spine.Entidades.Aud;
using Spine.Entidades.Cmn;
using Spine.Constantes.Codigos.Cfg;
using Spine.Constantes.Valores.Cfg;
using Spine.Librerias.Autenticacion;
using Spine.Librerias.Database;
using Spine.Librerias.General;
using Spine.Librerias.Negocio;
using System.Collections.Generic;

namespace Spine.Negocio.Cmn
{
    public class NegCategoria : NegBase
    {
        public NegCategoria(Sesion pobjSesion) : base(pobjSesion)
        {
        }

        public List<Categoria> Consultar(short piCtgId = -1, string psCtgNombre = "", short piCtgTipo = -1, short piCtgEstado = -1)
        {
            return new DaoCategoria(objSesion).Consultar(piCtgId: piCtgId, psCtgNombre: psCtgNombre, piCtgTipo: piCtgTipo, piCtgEstado: piCtgEstado);
        }

        public short Crear(Categoria pobjCategoria)
        {
            using (Conexion vobjConexion = ConexionFactory.Instanciar())
            {
                vobjConexion.BeginTransaction();

                new DaoCategoria(objSesion).Crear(vobjConexion, pobjCategoria);

                Cambio vobjCambio = new Cambio();
                vobjCambio.dCamFecha = Metodos.FechaSistema();
                vobjCambio.iUsuId = objSesion.iUsuId;
                vobjCambio.sEntCodigo = EntidadCod.CATEGORIA;
                vobjCambio.iCamRegistro = pobjCategoria.iCtgId;
                vobjCambio.iCamAccion = ValCatalogoItem.ACCION_CREACION;
                new CambioDao(objSesion).Crear(vobjConexion, vobjCambio);

                vobjConexion.Commit();

                return pobjCategoria.iCtgId;
            }
        }

        public short Editar(Categoria pobjCategoria)
        {
            using (Conexion vobjConexion = ConexionFactory.Instanciar())
            {
                vobjConexion.BeginTransaction();

                new DaoCategoria(objSesion).Editar(vobjConexion, pobjCategoria);

                Cambio vobjCambio = new Cambio();
                vobjCambio.dCamFecha = Metodos.FechaSistema();
                vobjCambio.iUsuId = objSesion.iUsuId;
                vobjCambio.sEntCodigo = EntidadCod.CATEGORIA;
                vobjCambio.iCamRegistro = pobjCategoria.iCtgId;
                vobjCambio.iCamAccion = ValCatalogoItem.ACCION_EDICION;
                new CambioDao(objSesion).Crear(vobjConexion, vobjCambio);

                vobjConexion.Commit();

                return pobjCategoria.iCtgId;
            }
        }

        public bool Activar(short piCtgId)
        {
            using (Conexion vobjConexion = ConexionFactory.Instanciar())
            {
                vobjConexion.BeginTransaction();

                new DaoCategoria(objSesion).CambiarEstado(vobjConexion, piCtgId, ValCatalogoItem.ESTADO_ACTIVADO);

                Cambio vobjCambio = new Cambio();
                vobjCambio.dCamFecha = Metodos.FechaSistema();
                vobjCambio.iUsuId = objSesion.iUsuId;
                vobjCambio.sEntCodigo = EntidadCod.CATEGORIA;
                vobjCambio.iCamRegistro = piCtgId;
                vobjCambio.iCamAccion = ValCatalogoItem.ACCION_ACTIVACION;
                new CambioDao(objSesion).Crear(vobjConexion, vobjCambio);

                vobjConexion.Commit();

                return true;
            }
        }

        public bool Desactivar(short piCtgId)
        {
            using (Conexion vobjConexion = ConexionFactory.Instanciar())
            {
                vobjConexion.BeginTransaction();

                new DaoCategoria(objSesion).CambiarEstado(vobjConexion, piCtgId, ValCatalogoItem.ESTADO_DESACTIVADO);

                Cambio vobjCambio = new Cambio();
                vobjCambio.dCamFecha = Metodos.FechaSistema();
                vobjCambio.iUsuId = objSesion.iUsuId;
                vobjCambio.sEntCodigo = EntidadCod.CATEGORIA;
                vobjCambio.iCamRegistro = piCtgId;
                vobjCambio.iCamAccion = ValCatalogoItem.ACCION_DESACTIVACION;
                new CambioDao(objSesion).Crear(vobjConexion, vobjCambio);

                vobjConexion.Commit();

                return true;
            }
        }
    }
}
