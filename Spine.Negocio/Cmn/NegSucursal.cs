﻿using Spine.Constantes.Codigos.Cfg;
using Spine.Constantes.Valores.Cfg;
using Spine.Datos.Aud;
using Spine.Datos.Cmn;
using Spine.Entidades.Aud;
using Spine.Entidades.Cmn;
using Spine.Librerias.Autenticacion;
using Spine.Librerias.Database;
using Spine.Librerias.General;
using Spine.Librerias.Negocio;
using System.Collections.Generic;

namespace Spine.Negocio.Cmn
{
    public class NegSucursal : NegBase
    {
        public NegSucursal(Sesion pobjSesion) : base(pobjSesion)
        {
        }

        public List<Sucursal> Consultar(short piSucId = -1, string psSucNombre = "", short piSucEstado = -1)
        {
            return new DaoSucursal(objSesion).Consultar(piSucId: piSucId, psSucNombre: psSucNombre, piSucEstado: piSucEstado);
        }

        public short Crear(Sucursal pobjSucursal)
        {
            using (Conexion vobjConexion = ConexionFactory.Instanciar())
            {
                vobjConexion.BeginTransaction();

                new DaoSucursal(objSesion).Crear(vobjConexion, pobjSucursal);

                Cambio vobjCambio = new Cambio();
                vobjCambio.dCamFecha = Metodos.FechaSistema();
                vobjCambio.iUsuId = objSesion.iUsuId;
                vobjCambio.sEntCodigo = EntidadCod.SUCURSAL;
                vobjCambio.iCamRegistro = pobjSucursal.iSucId;
                vobjCambio.iCamAccion = ValCatalogoItem.ACCION_CREACION;
                new CambioDao(objSesion).Crear(vobjConexion, vobjCambio);

                vobjConexion.Commit();

                return pobjSucursal.iSucId;
            }
        }

        public short Editar(Sucursal pobjSucursal)
        {
            using (Conexion vobjConexion = ConexionFactory.Instanciar())
            {
                vobjConexion.BeginTransaction();

                new DaoSucursal(objSesion).Editar(vobjConexion, pobjSucursal);

                Cambio vobjCambio = new Cambio();
                vobjCambio.dCamFecha = Metodos.FechaSistema();
                vobjCambio.iUsuId = objSesion.iUsuId;
                vobjCambio.sEntCodigo = EntidadCod.SUCURSAL;
                vobjCambio.iCamRegistro = pobjSucursal.iSucId;
                vobjCambio.iCamAccion = ValCatalogoItem.ACCION_EDICION;
                new CambioDao(objSesion).Crear(vobjConexion, vobjCambio);

                vobjConexion.Commit();

                return pobjSucursal.iSucId;
            }
        }

        public bool Activar(short piSucId)
        {
            using (Conexion vobjConexion = ConexionFactory.Instanciar())
            {
                vobjConexion.BeginTransaction();

                new DaoSucursal(objSesion).CambiarEstado(vobjConexion, piSucId, ValCatalogoItem.ESTADO_ACTIVADO);

                Cambio vobjCambio = new Cambio();
                vobjCambio.dCamFecha = Metodos.FechaSistema();
                vobjCambio.iUsuId = objSesion.iUsuId;
                vobjCambio.sEntCodigo = EntidadCod.SUCURSAL;
                vobjCambio.iCamRegistro = piSucId;
                vobjCambio.iCamAccion = ValCatalogoItem.ACCION_ACTIVACION;
                new CambioDao(objSesion).Crear(vobjConexion, vobjCambio);

                vobjConexion.Commit();

                return true;
            }
        }

        public bool Desactivar(short piSucId)
        {
            using (Conexion vobjConexion = ConexionFactory.Instanciar())
            {
                vobjConexion.BeginTransaction();

                new DaoSucursal(objSesion).CambiarEstado(vobjConexion, piSucId, ValCatalogoItem.ESTADO_DESACTIVADO);

                Cambio vobjCambio = new Cambio();
                vobjCambio.dCamFecha = Metodos.FechaSistema();
                vobjCambio.iUsuId = objSesion.iUsuId;
                vobjCambio.sEntCodigo = EntidadCod.SUCURSAL;
                vobjCambio.iCamRegistro = piSucId;
                vobjCambio.iCamAccion = ValCatalogoItem.ACCION_DESACTIVACION;
                new CambioDao(objSesion).Crear(vobjConexion, vobjCambio);

                vobjConexion.Commit();

                return true;
            }
        }
    }
}

