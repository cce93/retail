﻿using Spine.Datos.Lgt;
using Spine.Entidades.Lgt;
using Spine.Librerias.Autenticacion;
using Spine.Librerias.Database;
using Spine.Librerias.Negocio;
using System.Collections.Generic;

namespace Spine.Negocio.Lgt
{
    public class NegFacturaCompra : NegBase
    {
        public NegFacturaCompra(Sesion pobjSesion) : base(pobjSesion)
        {
        }

        public List<FacturaCompra> Consultar()
        {




            return null;
        }

        public int Crear(FacturaCompra pobjFactura)
        {
            using (var vobjConexion = ConexionFactory.Instanciar())
            {
                var vdaoFacturaCompra = new DaoFacturaCompra(objSesion);

                pobjFactura.iFacComId = vdaoFacturaCompra.Crear(vobjConexion, pobjFactura);

                if (pobjFactura.iFacComEstado == 2 && pobjFactura.lFacComGenerarOrdenMov) // Emitido
                {
                    // Generar movimiento de entrada
                
                }



                vobjConexion.Commit();
            }








            return 0;
        }

        public int Editar(FacturaCompra pobjFactura)
        {
            return 0;
        }
    }
}
