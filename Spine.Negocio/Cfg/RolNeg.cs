﻿using Spine.Entidades.Cfg;
using Spine.Entidades.Seg;
using Spine.Librerias.Autenticacion;
using Spine.Librerias.Negocio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spine.Negocio.Cfg
{
    public class RolNeg : NegBase
    {
        public RolNeg(Sesion pobjSesion) : base(pobjSesion)
        {
        }

        public List<Rol> Consultar(short piRolId = -1, string psRolCodigo = "", short piRolEstado = -1)
        {
            return null;
        }

        public bool Activar(short piRolId)
        {
            return false;
        }

        public bool Desactivar(short piRolId)
        {
            return false;
        }

        public List<RolOpcion> ConsultarOpciones(short piRolId)
        {
            return null;
        }
    }
}
