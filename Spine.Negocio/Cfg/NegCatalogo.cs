﻿using Spine.Datos.Cfg;
using Spine.Entidades.Cfg;
using Spine.Librerias.Autenticacion;
using Spine.Librerias.Negocio;
using System.Collections.Generic;

namespace Spine.Negocio.Cfg
{
    public class NegCatalogo : NegBase
    {
        public NegCatalogo(Sesion pobjSesion) : base(pobjSesion)
        {
        }

        public List<CatalogoItem> ConsultarItems(string psCatCodigo, short piCatIteEstado = -1)
        {
            return new DaoCatalogo(objSesion).ConsultarItems(psCatCodigo: psCatCodigo, piCatIteEstado: piCatIteEstado);
        }
    }
}
