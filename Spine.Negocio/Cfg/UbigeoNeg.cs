﻿using Spine.Datos.Cfg;
using Spine.Entidades.Cfg;
using Spine.Librerias.Autenticacion;
using Spine.Librerias.Negocio;
using System.Collections.Generic;


namespace Spine.Negocio.Cfg
{
    public class UbigeoNeg : NegBase
    {
        public UbigeoNeg(Sesion pobjSesion) : base(pobjSesion)
        {
        }

        public List<Ubigeo> Consultar(short piUbiId = -1, string psUbiRuta = "", short piUbiTipo = -1, short piUbiEstado = -1)
        {
            return new DaoUbigeo(objSesion).Consultar(piUbiId: piUbiId, psUbiRuta: psUbiRuta, piUbiTipo: piUbiTipo, piUbiEstado: piUbiEstado);
        }
    }
}