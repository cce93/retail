﻿using Spine.Datos.Cfg;
using Spine.Entidades.Cfg;
using Spine.Librerias.Autenticacion;
using Spine.Librerias.Negocio;
using System.Collections.Generic;

namespace Spine.Negocio.Cfg
{
    public class MonedaNeg : NegBase
    {
        public MonedaNeg(Sesion pobjSesion) : base(pobjSesion)
        {
        }

        public List<Moneda> Consultar(short piMonId = -1, short piMonEstado = -1)
        {
            return new DaoMoneda(objSesion).Consultar(piMonId: piMonId, piMonEstado: piMonEstado);
        }

    }
}
