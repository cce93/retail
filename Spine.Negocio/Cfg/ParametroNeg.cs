﻿using Spine.Datos.Cfg;
using Spine.Entidades.Cfg;
using Spine.Librerias.Autenticacion;
using Spine.Librerias.Negocio;

namespace Spine.Negocio.Cfg
{
    public class ParametroNeg : NegBase
    {
        public ParametroNeg(Sesion pobjSesion) : base(pobjSesion)
        {
        }

        public Parametro ConsultarUno(string psParCodigo)
        {
            return new DaoParametro(objSesion).ConsultarUno(psParCodigo);
        }
    }
}
