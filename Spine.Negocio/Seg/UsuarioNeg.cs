﻿using Spine.Librerias.Negocio;
using Spine.Librerias.General;
using Spine.Librerias.Autenticacion;
using Spine.Entidades.Seg;
using System.Collections.Generic;
using Spine.Datos.Seg;
using System.Linq;
using Spine.Constantes.Valores.Cfg;
using Spine.Entidades.Cfg;
using Spine.Datos.Cfg;

namespace Spine.Seg.Negocio
{
    public class UsuarioNeg : NegBase
    {
        public UsuarioNeg(Sesion pobjSesion) : base(pobjSesion)
        {
        }

        public Sesion Autenticar(Usuario pobjUsuario)
        {
            pobjUsuario = new UsuarioDao(objSesion).Consultar(psUsuUsuario: pobjUsuario.sUsuUsuario, psUsuClave: pobjUsuario.sUsuClave).FirstOrDefault();
            if (pobjUsuario == null)
            {
                throw Validacion.Excepcion("usuario y/o clave incorrectos");
            }
            else if (pobjUsuario.iUsuEstado == ValCatalogoItem.ESTADO_DESACTIVADO)
            {
                throw Validacion.Excepcion("usuario se encuentra desactivado");
            }
            else
            {
                Empresa vobjEmpresa = new DaoEmpresa(objSesion).Consultar(piEmpEstado: ValCatalogoItem.ESTADO_ACTIVADO).FirstOrDefault();
                if (vobjEmpresa == null)
                {
                    throw Validacion.Excepcion("No existe una empresa configurada");
                }
                else
                {
                    Sesion vobjSesion = new Sesion();
                    vobjSesion.iUsuId = pobjUsuario.iUsuId;
                    vobjSesion.sUsuUsuario = pobjUsuario.sUsuUsuario;
                    vobjSesion.iSucId = pobjUsuario.iSucId;
                    vobjSesion.sSucNombre = pobjUsuario.sSucNombre;
                    vobjSesion.iPerId = pobjUsuario.iPerId;
                    vobjSesion.sPerNombre = pobjUsuario.sPerNombre;

                    vobjSesion.iEmpId = vobjEmpresa.iEmpId;
                    vobjSesion.sEmpRuc = vobjEmpresa.sEmpRuc;
                    vobjSesion.sEmpRazonSocial = vobjEmpresa.sEmpRazonSocial;
                    vobjSesion.sEmpRazonComercial = vobjEmpresa.sEmpRazonComercial;
                    vobjEmpresa.sEmpLogoRuta = vobjEmpresa.sEmpLogoRuta;

                    return vobjSesion;
                }
            }
        }

        public List<Usuario> Consultar(
            int piUsuId = -1, string psUsuUsuario = "", string psUsuCorreo = "", short piSucId = -1,
            int piPerId = -1, short piUsuEstado = -1
        )
        {
            return new UsuarioDao(objSesion).Consultar(
                    piUsuId: piUsuId, psUsuUsuario: psUsuUsuario, psUsuClave: "", psUsuCorreo: psUsuCorreo,
                    piSucId: piSucId, piPerId: piPerId, piUsuEstado: piUsuEstado
                );
        }

        public int Crear(Usuario pobjUsuario)
        {
            return 1;
        }

        public int Editar(Usuario pobjUsuario)
        {
            return 1;
        }

        public bool Activar(int piUsuId)
        {
            return true;
        }

        public bool Desactivar(int piUsuId)
        {
            return true;
        }

        public List<UsuarioRol> ConsultarRoles(int piUsuId)
        {
            return null;
        }

        public bool GuardarRoles(Usuario pobjUsuario)
        {
            return true;
        }
    }
}