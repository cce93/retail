﻿using Spine.Entidades.Com;
using Spine.Librerias.Autenticacion;
using Spine.Librerias.Negocio;
using System.Collections.Generic;

namespace Spine.Negocio.Com
{
    public class NegNotaCreditoVenta : NegBase
    {
        public NegNotaCreditoVenta(Sesion pobjSesion) : base(pobjSesion)
        {
        }

        public List<NotaCreditoVenta> Consultar()
        {
            return null;
        }

        public int Crear(NotaCreditoVenta pobjNotaCredito)
        {
            return 0;
        }

        public int Enviar(int piNotCreId)
        {
            return 0;
        }
    }
}
