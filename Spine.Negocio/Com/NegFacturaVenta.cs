﻿using Spine.Librerias.Negocio;
using Spine.Librerias.Autenticacion;
using System.Collections.Generic;
using Spine.Entidades.Com;

namespace Spine.Negocio.Com
{
    public class NegFacturaVenta : NegBase
    {
        public NegFacturaVenta(Sesion pobjSesion) : base(pobjSesion)
        {
        }

        public List<FacturaVenta> Consultar()
        {
            return null;
        }

        public int Crear(FacturaVenta pobjFactura)
        {
            return 0;
        }

        public int Enviar(int piFacId)
        {
            return 0;
        }
    }
}