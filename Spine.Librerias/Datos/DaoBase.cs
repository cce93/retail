﻿using Spine.Librerias.Autenticacion;

namespace Spine.Librerias.Datos
{
    public class DaoBase
    {
        protected Sesion objSesion { private set; get; }

        public DaoBase(Sesion pobjSesion)
        {
            objSesion = pobjSesion;
        }
    }
}
