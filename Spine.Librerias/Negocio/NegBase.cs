﻿using Spine.Librerias.Autenticacion;

namespace Spine.Librerias.Negocio
{
    public class NegBase
    {
        protected Sesion objSesion { private set; get; }

        public NegBase(Sesion pobjSesion)
        {
            objSesion = pobjSesion;
        }
    }
}
