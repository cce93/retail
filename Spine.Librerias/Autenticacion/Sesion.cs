﻿using System.Security.Principal;

namespace Spine.Librerias.Autenticacion
{
    public class Sesion : IIdentity
    {
        // Atributos
        public byte iEmpId { set; get; }
        public string sEmpRuc { set; get; }
        public string sEmpRazonSocial { set; get; }
        public string sEmpRazonComercial { set; get; }
        public string sEmpLogoRuta { set; get; }

        public int iUsuId { set; get; }
        public string sUsuUsuario { set; get; }

        public int iPerId { set; get; }
        public string sPerNombre { set; get; }

        public short iSucId { set; get; }
        public string sSucNombre { set; get; }

        public bool lSesCambiarClave { set; get; }

        // Atributos heredados
        public string Name { set; get; }
        public string AuthenticationType { get { return "Token"; } }
        public bool IsAuthenticated { get { return true; } }
    }
}
