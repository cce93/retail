﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spine.Librerias.General
{
    public static class Metodos
    {
        public static string ToJson<T>(T poObjeto, bool plFormatear = true)
        {
            if (plFormatear)
                return JsonConvert.SerializeObject(poObjeto, Newtonsoft.Json.Formatting.Indented, new JsonSerializerSettings()
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
            else
                return JsonConvert.SerializeObject(poObjeto, new JsonSerializerSettings()
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
        }

        public static DateTime FechaSistema()
        {
            return TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.Now, TimeZoneInfo.Local.Id, "SA Pacific Standard Time");
        }
    }
}
