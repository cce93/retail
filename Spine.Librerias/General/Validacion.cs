﻿using System;


namespace Spine.Librerias.General
{
    public static class Validacion
    {
        public static Exception Excepcion(string psMensaje)
        {
            var vobjExcepcion = new Exception(psMensaje);
            vobjExcepcion.Data["lEsVal"] = true;
            return vobjExcepcion;
        }
    }
}
