﻿using Spine.Librerias.Autenticacion;
using Spine.Librerias.General;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Spine.Librerias.Web
{
    public class WebBaseController : Controller
    {

        //protected Sesion objSesion { get { return Session["objSesion"] != null ? (ISesion)Session[CnsWeb.SESION] : null; } }
        protected Sesion objSesion = new Sesion();
        //protected ActionResult ViewError(Exception pobjExcepcion)
        //{
        //    string vsMensaje = string.Empty;
        //    long vnIdLog = Metodos.GetLog(pobjExcepcion); // Obtenemos nIdLog de Data de excepción
        //    if (vnIdLog == 0)
        //        vsMensaje = pobjExcepcion.Message;
        //    else
        //        vsMensaje = "Lo sentimos, se ha producido un error al ejecutar operación (" + vnIdLog + ").";

        //    return new RedirectResult(GetUrlBase(Request.Url.ToString()) + "Comun/Error?psMensaje=" + vsMensaje);
        //}
        //protected ContentResult JsonExito(string psContenido)
        //{
        //    return Content(psContenido, "application/json", Encoding.UTF8);
        //}
        protected ContentResult JsonExito<T>(T pobjObjeto)
        {
            // return JsonExito(Metodos.ToJson(ApiRespuestaFactory.Exito(pobjObjeto)));

            return Content(Metodos.ToJson(new ApiRespuesta<T>(pobjObjeto)), "application/json", Encoding.UTF8);
        }
        protected ContentResult JsonError(Exception pobjExcepcion)
        {
            return Content(Metodos.ToJson(false), "application/json", Encoding.UTF8);
        }

        //protected T GetDatos<T>(string psCadena)
        //{
        //    ApiRespuesta<T> vobjApiRpta = Metodos.FromJson<ApiRespuesta<T>>(psCadena);
        //    if (vobjApiRpta.nTipo == (byte)0)
        //    {
        //        Exception vobjExcepcion = new Exception(vobjApiRpta.sMensaje + (vobjApiRpta.nError > 0 ? " (" + vobjApiRpta.nError + ")." : ""));
        //        if (vobjApiRpta.nError > 0)
        //            vobjExcepcion.Data["nIdLogAplicacion"] = vobjApiRpta.nError;
        //        throw vobjExcepcion;
        //    }
        //    else if (vobjApiRpta.nTipo == (byte)2)
        //        Metodos.LanzarVal(vobjApiRpta.sMensaje);
        //    return vobjApiRpta.objDatos;
        //}

        //protected FileContentResult DescargarArchivo(byte[] parrArchivo, string psArchivoContenido = "", string psArchivoNombre = "")
        //{
        //    if (psArchivoContenido == "")
        //        psArchivoContenido = CnsWeb.CONTENT_OCTET;

        //    if (psArchivoNombre == "")
        //        return File(parrArchivo, psArchivoContenido);
        //    else
        //        return File(parrArchivo, psArchivoContenido, psArchivoNombre);
        //}
        //protected FileContentResult DescargarArchivo(string psArchivoRuta, string psArchivoContenido = "", string psArchivoNombre = "")
        //{
        //    return DescargarArchivo(Metodos.LeerArchivo(psArchivoRuta), psArchivoContenido, psArchivoNombre);
        //}

        //public static string GetUrlBase(string psUrlAbsoluta)
        //{
        //    // Variables :
        //    string vsHttp = string.Empty;
        //    string[] varrSegmentos = new string[] { };
        //    string vsUrlBase = string.Empty;

        //    // Procesamiento
        //    byte vnSegmentosBase = Convert.ToByte(ConfigGlobal.WEB_URL_SEGMENTOS);

        //    vsHttp = (psUrlAbsoluta.Contains("https://") ? "https://" : "http://");
        //    psUrlAbsoluta = psUrlAbsoluta.Replace(vsHttp, string.Empty); // Quitamos http
        //    varrSegmentos = psUrlAbsoluta.Split('/');

        //    for (byte i = 0; i < varrSegmentos.Length; i++)
        //    {
        //        if (i < vnSegmentosBase)
        //            vsUrlBase += varrSegmentos[i] + "/";
        //        else
        //            break;
        //    }

        //    vsUrlBase = vsHttp + vsUrlBase;
        //    return vsUrlBase;
        //}
    }
}
