﻿using Spine.Librerias.Autenticacion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Spine.Librerias.Web
{
    public class AutorizaAttribute : AuthorizeAttribute
    {
        #region Variables
        private bool lValidarUrl = false;
        private bool lSesionCorrecta = false;
        private bool lTieneAccesoUrl = false;
        private string sErrorAutorizacion = string.Empty;
        private string sUrlBase = string.Empty;
        #endregion

        #region Constructores
        public AutorizaAttribute()
        {
            lValidarUrl = false;
            lSesionCorrecta = false;
            lTieneAccesoUrl = false;
        }
        public AutorizaAttribute(bool plValidarUrl)
        {
            lValidarUrl = plValidarUrl;
            lSesionCorrecta = false;
            lTieneAccesoUrl = false;
        }
        #endregion

        #region Métodos
        protected override bool AuthorizeCore(HttpContextBase pobjHttpContext)
        {
            try
            {
                //lSesionCorrecta = false;
                //lTieneAccesoUrl = false;
                //sUrlBase = WebController.GetUrlBase(pobjHttpContext.Request.Url.ToString());

                //// Proceso para obtener información de sesión
                //// ************************************************************************************************************
                //Sesion vobjSesionCookie = null;
                //HttpCookie vobjCookie = pobjHttpContext.Request.Cookies[CnsWeb.COOKIE];
                //if (vobjCookie != null)
                //{
                //    string vsToken = vobjCookie[CnsWeb.COOKIE_TOKEN];
                //    if (!string.IsNullOrEmpty(vsToken))
                //        vobjSesionCookie = SesionFactory.Desencriptar(vsToken);
                //}

                //ISesion vobjSesionServer = null;
                //if (pobjHttpContext.Session[CnsWeb.SESION] != null)
                //    vobjSesionServer = (ISesion)pobjHttpContext.Session[CnsWeb.SESION];

                //// Proceso para verificación de sesión
                //// ************************************************************************************************************
                //if (vobjSesionCookie != null && vobjSesionServer != null)
                //{
                //    if (vobjSesionServer.nIdSesion != vobjSesionCookie.nIdSesion)
                //        ReconstruirSesion(pobjHttpContext, vobjCookie, vobjSesionCookie);
                //    lSesionCorrecta = true;
                //}
                //else if (vobjSesionCookie != null && vobjSesionServer == null)
                //{
                //    ReconstruirSesion(pobjHttpContext, vobjCookie, vobjSesionCookie);
                //    lSesionCorrecta = true;
                //}
                //else
                //{
                //    lSesionCorrecta = false;
                //}

                //// Proceso para verificación de acceso a formulario
                //// ************************************************************************************************************
                //if (lSesionCorrecta)
                //{
                //    if (lValidarUrl)
                //    {
                //        vobjSesionServer = (ISesion)pobjHttpContext.Session[CnsWeb.SESION];

                //        lTieneAccesoUrl = true;
                //        if (ConfigGlobal.WEB_PERMISO_VALIDAR.Equals("1"))
                //        {
                //            string vsForRuta = pobjHttpContext.Request.Url.ToString().Replace(sUrlBase, string.Empty);
                //            MRolFormulario vobjRolFormulario = GetRolFormulario(vobjSesionServer, vsForRuta);
                //            if (vobjRolFormulario == null)
                //            {
                //                lTieneAccesoUrl = false;
                //            }
                //            else
                //            {
                //                lTieneAccesoUrl = true;
                //                pobjHttpContext.Session[CnsWeb.SESION_ROL_FORMULARIO] = vobjRolFormulario;
                //            }
                //        }
                //    }
                //    else
                //    {
                //        lTieneAccesoUrl = true;
                //    }
                //}
            }
            catch (Exception)
            {
                //Metodos.LogError("AutorizaAttribute.AuthorizeCore()", vobjExcepcion.Message);
                //Metodos.LogError("AutorizaAttribute.AuthorizeCore()", vobjExcepcion.StackTrace);
                //DepuradorFactory.Instanciar().GuardarError(SesionFactory.Instanciar(), vobjExcepcion, "AutorizaAttribute.AuthorizeCore()");
                //sErrorAutorizacion = vobjExcepcion.Message;
                //lSesionCorrecta = false;
                //lTieneAccesoUrl = false;
            }
            return (lSesionCorrecta && lTieneAccesoUrl);
        }
        protected override void HandleUnauthorizedRequest(AuthorizationContext pobjFilterContext)
        {
            if (!string.IsNullOrEmpty(sErrorAutorizacion))
                pobjFilterContext.Result = new RedirectResult(sUrlBase + "Comun/Error?psMensaje=" + sErrorAutorizacion);
            else if (!lSesionCorrecta)
                pobjFilterContext.Result = new RedirectResult(sUrlBase + "Comun/Expirado");
            else
                pobjFilterContext.Result = new RedirectResult(sUrlBase + "Comun/Bloqueado");
        }
        //protected virtual void ReconstruirSesion(HttpContextBase pobjHttpContext, HttpCookie pobjCookie, ISesion pobjSesionCookie)
        //{
        //    try
        //    {
        //        // Reconstruimos sesión
        //        pobjHttpContext.Session[CnsWeb.SESION] = pobjSesionCookie;

        //        if (!string.IsNullOrEmpty(ConfigGlobal.WEB_APLICACION_CODIGO) && ConfigGlobal.WEB_MENU_COSULTAR.Equals("1"))
        //        {
        //            // Consultamos menú de usuario por canal
        //            List<MMenu> vlstDatos = ConsultarMenu(pobjSesionCookie);

        //            // Obtenemos elementos a guardar en cookie
        //            string vsMenusHtml = MMenu.ObtenerMenuHtml(sUrlBase, vlstDatos);

        //            pobjHttpContext.Session[CnsWeb.SESION_MENU] = vsMenusHtml;
        //        }
        //    }
        //    catch (Exception vobjException)
        //    {
        //        throw DepuradorFactory.Instanciar().GuardarError(pobjSesionCookie, vobjException, "AutorizaAttribute.ReconstruirSesion()", pobjSesionCookie);
        //    }
        //}
        //protected virtual List<MMenu> ConsultarMenu(ISesion pobjSesion)
        //{
        //    List<MMenu> vlstRpta = new List<MMenu>();
        //    if (ConfigGlobal.WEB_MENU_LOCAL.Equals("1")) // local
        //    {
        //        Conexion vobjConexion = null;
        //        try
        //        {
        //            vobjConexion = ConexionFactory.Instanciar();
        //            vlstRpta = vobjConexion.EjecutarConsultaStored<MMenu>(
        //                    ConfigGlobal.WEB_MENU_SP,
        //                    new Parametro("pnIdEmpresa", pobjSesion.nIdEmpresa),
        //                    new Parametro("pnIdUsuario", pobjSesion.nIdUsuario),
        //                    new Parametro("psAplCodigo", ConfigGlobal.WEB_APLICACION_CODIGO)
        //                );
        //            if (vlstRpta.Count > 0)
        //                vlstRpta = Metodos.LlenarDetalleRecursivo(vlstRpta, "nIdMenu", "nIdMenuPadre", "lstMenu");
        //        }
        //        catch (Exception vobjExcepcion)
        //        {
        //            throw DepuradorFactory.Instanciar().GuardarError(pobjSesion, vobjExcepcion, "AutorizaAttribute.ConsultarMenu()");
        //        }
        //        finally
        //        {
        //            if (vobjConexion != null)
        //                vobjConexion.Close();
        //        }
        //    }
        //    else
        //    {
        //        if (string.IsNullOrEmpty(ConfigGlobal.WEB_MENU_DOMINIO))
        //            throw new Exception("Asegúrese de configurar 'WEB_CONSULTAR_MENU_DOMINIO'");

        //        string vsAccion = "Seg/Usuario/ConsultarMenu/{pnIdEmpresa}/{pnIdUsuario}/{psAplCodigo}";
        //        vsAccion = vsAccion.Replace("{pnIdEmpresa}", pobjSesion.nIdEmpresa.ToString());
        //        vsAccion = vsAccion.Replace("{pnIdUsuario}", pobjSesion.nIdUsuario.ToString());
        //        vsAccion = vsAccion.Replace("{psAplCodigo}", ConfigGlobal.WEB_APLICACION_CODIGO);

        //        string vsRpta = RestManagerFactory.Instanciar().Get(
        //                ConfigGlobal.WEB_MENU_DOMINIO,
        //                vsAccion,
        //                SesionFactory.Encriptar(pobjSesion)
        //            );

        //        return Metodos.FromJson<ApiRespuesta<List<MMenu>>>(vsRpta).objDatos;

        //    }
        //    return vlstRpta;
        //}
        //protected virtual MRolFormulario GetRolFormulario(ISesion pobjSesion, string psForRuta)
        //{
        //    MRolFormulario vobjPermiso = new MRolFormulario();
        //    if (ConfigGlobal.WEB_MENU_LOCAL.Equals("1")) // local
        //    {
        //        Conexion vobjConexion = null;
        //        try
        //        {
        //            vobjConexion = ConexionFactory.Instanciar();
        //            return vobjConexion.EjecutarConsultaStored<MRolFormulario>(
        //                    ConfigGlobal.WEB_MENU_SP,
        //                    new Parametro("pnIdEmpresa", pobjSesion.nIdEmpresa),
        //                    new Parametro("pnIdUsuario", pobjSesion.nIdUsuario),
        //                    new Parametro("psForRuta", psForRuta)
        //                ).FirstOrDefault();
        //        }
        //        catch (Exception vobjExcepcion)
        //        {
        //            throw DepuradorFactory.Instanciar().GuardarError(pobjSesion, vobjExcepcion, "AutorizaAttribute.GetRolFormulario()");
        //        }
        //        finally
        //        {
        //            if (vobjConexion != null)
        //                vobjConexion.Close();
        //        }
        //    }
        //    else
        //    {
        //        try
        //        {
        //            if (string.IsNullOrEmpty(ConfigGlobal.WEB_MENU_DOMINIO))
        //                throw new Exception("Asegúrese de configurar 'WEB_CONSULTAR_MENU_DOMINIO'");

        //            string vsRpta = RestManagerFactory.Instanciar().Get(
        //                    ConfigGlobal.WEB_MENU_DOMINIO,
        //                    "Seg/Usuario/ConsultarMenu",
        //                    SesionFactory.Encriptar(pobjSesion),
        //                    new Parametro("pnIdEmpresa", pobjSesion.nIdEmpresa),
        //                    new Parametro("pnIdUsuario", pobjSesion.nIdUsuario),
        //                    new Parametro("psForRuta", psForRuta)
        //                );

        //            return Metodos.FromJson<ApiRespuesta<List<MRolFormulario>>>(vsRpta).objDatos.FirstOrDefault();
        //        }
        //        catch (Exception vobjExcepcion)
        //        {
        //            throw DepuradorFactory.Instanciar().GuardarError(pobjSesion, vobjExcepcion, "AutorizaAttribute.GetPermiso()");
        //        }
        //    }
        //}
        #endregion
    }
}
