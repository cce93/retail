﻿namespace Spine.Librerias.Web
{
    public class ApiRespuesta<T>
    {
        public ApiRespuesta(T pobjDatos) { iTipo = 1; objDatos = pobjDatos; }

        public byte iTipo { set; get; }
        public string sMensaje { set; get; }
        public T objDatos { set; get; }
    }
}
